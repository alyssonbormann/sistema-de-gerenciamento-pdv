﻿using System.Collections.Generic;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IRepositoryBase<T>  where T: class
    {
        void Insert(T obj);
        void Update(T obj);
        void Delete(T obj);
        T GetFindCodigo(int codigo);
        IEnumerable<T> GetAll();
        void Dispose();
    }
}
