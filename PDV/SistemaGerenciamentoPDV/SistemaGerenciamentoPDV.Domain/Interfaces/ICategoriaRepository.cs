﻿using SistemaGerenciamentoPDV.Domain.Entities;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface ICategoriaRepository: IRepositoryBase<Categoria>
    {
    }
}
