﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("subcategoria")]
    public class SubCategoria
    {
        [Column("scat_cod")]
        [Key]
        public int SubCategoria_Codigo { get; set; }

        [Column("scat_nome")]
        public string SubCategoria_Nome { get; set; }

        [Column("cat_cod")]
        public int Categoria_Codigo { get; set; }
    }
}
