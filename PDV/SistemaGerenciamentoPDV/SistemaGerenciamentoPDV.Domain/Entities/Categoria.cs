﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("categoria")]
    public class Categoria
    {
        [Column("cat_cod")]
        [Key]
        public int Categoria_Codigo { get; set; }
        [Column("cat_nome")]
        public string Categoria_Nome { get; set; }

        [NotMapped]
        public virtual ICollection<SubCategoria> SubCategorias { get; set; }
    }
}
