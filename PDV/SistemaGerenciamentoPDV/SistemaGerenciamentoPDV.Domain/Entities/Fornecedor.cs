﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("fornecedor")]
    public class Fornecedor
    {
        [Key]
        [Display(Name ="Código")]
        [Column("for_cod")]
        public int Fornecedor_Codigo { get; set; }

        [Display(Name = "Nome")]
        [Column("for_nome")]
        public string Fornecedor_Nome { get; set; }

        [Display(Name = "Razão Social")]
        [Column("for_rsocial")]
        public string Fornecedor_RazaoSocial { get; set; }

        [Display(Name = "IE")]
        [Column("for_ir")]
        public string Fornecedor_IE { get; set; }

        [Display(Name = "CNPJ")]
        [Column("for_cnpj")]
        public string Fornecedor_Cnpj { get; set; }

        [Display(Name = "CEP")]
        [Column("cli_cep")]
        public string Cliente_Cep { get; set; }

        [Display(Name = "Endereço")]
        [Column("cli_endereco")]
        public string Cliente_Endereco { get; set; }

        [Display(Name = "Bairro")]
        [Column("cli_bairro")]
        public string Cliente_Bairro { get; set; }

        [Display(Name = "Telefone")]
        [Column("cli_fone")]
        public string Cliente_Fone { get; set; }

        [Display(Name = "Celular")]
        [Column("cli_cel")]
        public string Cliente_Celular { get; set; }

        [Display(Name = "E-Mail")]
        [Column("cli_email")]
        public string Cliente_Email { get; set; }

        [Display(Name = "Número")]
        [Column("cli_endnumero")]
        public string Cliente_NumeroEndereco { get; set; }

        [Display(Name = "Cidade")]
        [Column("cli_cidade")]
        public string Cliente_Cidade { get; set; }

        [Display(Name = "Estado")]
        [Column("cli_estado")]
        public string Cliente_Estado { get; set; }
    }
}
