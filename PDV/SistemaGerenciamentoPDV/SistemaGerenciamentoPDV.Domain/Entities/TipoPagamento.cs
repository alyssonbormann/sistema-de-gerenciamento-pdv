﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("tipopagamento")]
    public class TipoPagamento
    {
        [Display(Name ="Código")]
        [Column("tpa_cod")]
        [Key]
        public int TipoPagamento_Codigo { get; set; }

        [Display(Name ="Tipo de Pagamento")]
        [Column("tpa_nome")]
        public string TipoPagamento_Nome { get; set; }
    }
}
