﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioTipoPagamento;

namespace SistemaGerenciamentoPDV.Presentation.Formularios
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public MainForm()
        {
            InitializeComponent();
            UserLookAndFeel defaultLF = UserLookAndFeel.Default;
            defaultLF.UseWindowsXPTheme = false;
            defaultLF.Style = LookAndFeelStyle.Skin;
            defaultLF.SkinName = "Office 2016 Colorful";
        }
        void navBarControl_ActiveGroupChanged(object sender, DevExpress.XtraNavBar.NavBarGroupEventArgs e)
        {
            navigationFrame.SelectedPageIndex = navBarControl.Groups.IndexOf(e.Group);
        }
        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void bbiFormasPagamento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormTipoPagamento f = new FormTipoPagamento();
            f.ShowDialog();
            f.Dispose();
        }
    }
}