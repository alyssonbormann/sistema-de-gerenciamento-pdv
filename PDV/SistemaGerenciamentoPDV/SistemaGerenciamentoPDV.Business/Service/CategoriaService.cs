﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class CategoriaService
    {
        private readonly CategoriaRepository _categoriaRepository = new CategoriaRepository();

        public void Inserir(Categoria categoria)
        {
            if (categoria.Categoria_Nome.Trim().Length == 0)
            {
                throw new Exception("Nome da categoria obrigatório");
            }
            _categoriaRepository.Insert(categoria);
        }

        public void Alterar(Categoria categoria)
        {
            if (categoria.Categoria_Codigo <= 0 )
            {
                throw new Exception("Código da categoria obrigatório");
            }
            if (categoria.Categoria_Nome.Trim().Length == 0)
            {
                throw new Exception("Nome da categoria obrigatório");
            }
            _categoriaRepository.Update(categoria);
        }

        public void Excluir(int codigo)
        {
            var codigoCategoria = _categoriaRepository.GetFindCodigo(codigo);
            _categoriaRepository.Delete(codigoCategoria);
        }

        public IEnumerable<Categoria> BuscarTodos()
        {
            return _categoriaRepository.GetAll();
        }

        public Categoria BuscarPorCodigo(int codigo)
        {
            return _categoriaRepository.GetFindCodigo(codigo);
        }

    }
}
