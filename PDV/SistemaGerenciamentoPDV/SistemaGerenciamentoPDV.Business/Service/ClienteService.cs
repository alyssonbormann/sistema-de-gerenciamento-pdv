﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class ClienteService
    {
        private readonly ClienteRepository _repository = new ClienteRepository();

        public void Incluir(Cliente obj)
        {
            if (obj.Cliente_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do cliente é obrigatório");
            }
            if (obj.Cliente_Cpf_Cnpj.Trim().Length == 0)
            {
                throw new Exception("O CPF/CNPJ do cliente é obrigatório.");
            }

            if (obj.Cliente_Rg_Ie.Trim().Length == 0)
            {
                throw new Exception("O RG/IE do cliente é obrigatório.");
            }
            if (obj.Cliente_Fone.Trim().Length == 0)
            {
                throw new Exception("O telefone do cliente é obrigatório");
            }

            //Cliente_Tipo = 0 - Fisica
            //Cliente_Tipo = 1 - Juridica
            _repository.Insert(obj);
        }
        public void Alterar(Cliente obj)
        {
            if (obj.Cliente_Codigo <= 0)
            {
                throw new Exception("O código do cliente é obrigatório");
            }
            if (obj.Cliente_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do cliente é obrigatório");
            }
            if (obj.Cliente_Cpf_Cnpj.Trim().Length == 0)
            {
                throw new Exception("O CPF/CNPJ do cliente é obrigatório.");
            }

            if (obj.Cliente_Rg_Ie.Trim().Length == 0)
            {
                throw new Exception("O RG/IE do cliente é obrigatório.");
            }
            if (obj.Cliente_Fone.Trim().Length == 0)
            {
                throw new Exception("O telefone do cliente é obrigatório");
            }
            _repository.Update(obj);
        }

        public IEnumerable<Cliente> BuscarTodos()
        {
            return _repository.GetAll();
        }
        public Cliente BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }

        public void Excluir(int codigo)
        {
            var codigoCliente = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoCliente);
        }
    }
}
