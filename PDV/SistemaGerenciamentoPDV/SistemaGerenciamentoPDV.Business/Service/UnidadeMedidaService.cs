﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class UnidadeMedidaService
    {
        private readonly UnidadeMedidaRepository _unidadeMedidaRepository = new UnidadeMedidaRepository();

        public void Inserir(UnidadeMedida unidadeMedida)
        {
            if (unidadeMedida.UnidadeMedida_Nome.Trim().Length == 0)
            {
                throw new Exception("Nome da unidade de medida obrigatório");
            }

            _unidadeMedidaRepository.Insert(unidadeMedida);
        }
        public void Alterar(UnidadeMedida unidadeMedida)
        {
            if (unidadeMedida.UnidadeMedida_Codigo <= 0)
            {
                throw new Exception("Código da unidade de medida obrigatório");
            }
            if (unidadeMedida.UnidadeMedida_Nome.Trim().Length == 0)
            {
                throw new Exception("Nome da unidade de medida é obrigátorio");
            }

            _unidadeMedidaRepository.Update(unidadeMedida);
        }

        public void Excluir(int codigo)
        {
            var codigoUnidadeMedida = _unidadeMedidaRepository.GetFindCodigo(codigo);
            _unidadeMedidaRepository.Delete(codigoUnidadeMedida);
        }

        public IEnumerable<UnidadeMedida> BuscarTodos()
        {
            return _unidadeMedidaRepository.GetAll();
        }

        public UnidadeMedida BuscarPorCodigo(int codigo)
        {
            return _unidadeMedidaRepository.GetFindCodigo(codigo);
        }

    }
}
