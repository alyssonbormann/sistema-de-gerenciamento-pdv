﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class TipoPagamentoService
    {
        private readonly TipoPagamentoRepository _repository = new TipoPagamentoRepository();

        public void Inserir(TipoPagamento obj)
        {
            if (obj.TipoPagamento_Nome.Trim().Length == 0)
            {
                throw new Exception("Tipo de pagamento é obrigatório.");
            }
            _repository.Insert(obj);
        }
        public void Alterar(TipoPagamento obj)
        {
            if (obj.TipoPagamento_Codigo <= 0)
            {
                throw new Exception("Código do pagamento é obrigatório.");
            }
            if (obj.TipoPagamento_Nome.Trim().Length == 0)
            {
                throw new Exception("Tipo de pagamento é obrigatório.");
            }
            _repository.Update(obj);
        }
        public void Excluir(int codigo)
        {
            var codigoTipoPagamento = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoTipoPagamento);
        }
        public IEnumerable<TipoPagamento> BuscarTodos()
        {
            return _repository.GetAll();
        }
        public TipoPagamento BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }
    }
}
