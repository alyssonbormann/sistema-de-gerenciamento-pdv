﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class FornecedorService
    {
        private readonly FornecedorRepository _repository = new FornecedorRepository();

        public void Incluir(Fornecedor obj)
        {
            if (obj.Fornecedor_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A razão social do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_IE.Trim().Length == 0)
            {
                throw new Exception("O IE do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_Cnpj.Trim().Length == 0)
            {
                throw new Exception("O CNPJ do fornecedor é obrigatório.");
            }
            if (obj.Cliente_Fone.Trim().Length == 0)
            {
                throw new Exception("O telefone do cliente é obrigatório");
            }

            _repository.Insert(obj);
        }

        public void Alterar(Fornecedor obj)
        {
            if (obj.Fornecedor_Codigo <= 0)
            {
                throw new Exception("O código do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_RazaoSocial.Trim().Length == 0)
            {
                throw new Exception("A razão social do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_IE.Trim().Length == 0)
            {
                throw new Exception("O IE do fornecedor é obrigatório.");
            }
            if (obj.Fornecedor_Cnpj.Trim().Length == 0)
            {
                throw new Exception("O CNPJ do fornecedor é obrigatório.");
            }
            if (obj.Cliente_Fone.Trim().Length == 0)
            {
                throw new Exception("O telefone do cliente é obrigatório");
            }
            _repository.Update(obj);
        }

        public IEnumerable<Fornecedor> BuscarTodos()
        {
            return _repository.GetAll();
        }
        public Fornecedor BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }
        public void Excluir(int codigo)
        {
            var codigoFornecedor = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoFornecedor);
        }

    }
}
