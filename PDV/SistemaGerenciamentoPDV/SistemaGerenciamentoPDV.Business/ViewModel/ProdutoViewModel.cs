﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.ViewModel
{
    public class ProdutoViewModel
    {
        [Display(Name = "Código")]
        public int Produto_Codigo { get; set; }
        [Display(Name = "Produto")]
        public string Produto_Nome { get; set; }
        [Display(Name = "Descrição")]
        public string Produto_Descricao { get; set; }
        //public string Produto_Foto { get; set; }
        [Display(Name = "Valor Pago")]
        public decimal Produto_ValorPago { get; set; }
        [Display(Name = "Valor Venda")]
        public decimal Produto_ValorVenda { get; set; }
        [Display(Name = "Quantidade")]
        public double Produto_Quantidade { get; set; }
        //public int UnidadeMedida_Codigo { get; set; }
        [Display(Name = "Unid. Medida")]
        public string UnidadeMedida_Nome { get; set; }
        //public int Categoria_Codigo { get; set; }
        [Display(Name = "Categoria")]
        public string Categoria_Nome { get; set; }
        //public int SubCategoria_Codigo { get; set; }
        [Display(Name = "SubCategoria")]
        public string SubCategoria_Nome { get; set; }



    }
}
