﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.ViewModel
{
    public class SubCategoriaViewModel
    {
        public int SubCategoria_Codigo { get; set; }
        public string SubCategoria_Nome { get; set; }
        public int Categoria_Codigo { get; set; }
        public string Categoria_Nome { get; set; }
    }
}
