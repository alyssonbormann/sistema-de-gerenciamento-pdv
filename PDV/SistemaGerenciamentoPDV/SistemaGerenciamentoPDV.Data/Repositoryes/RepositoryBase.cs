﻿using SistemaGerenciamento.Data.EntityContext;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class RepositoryBase<T>: IDisposable, IRepositoryBase<T> where T : class
    {
        protected Contexto Db = new Contexto();

        public void Delete(T obj)
        {
            Db.Set<T>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetAll()
        {
            return Db.Set<T>().ToList();
        }

        public T GetFindCodigo(int codigo)
        {
            return Db.Set<T>().Find(codigo);
        }

        public void Insert(T obj)
        {
            Db.Set<T>().Add(obj);
            Db.SaveChanges();
        }

        public void Update(T obj)
        {
            Db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
        }
    }
}
