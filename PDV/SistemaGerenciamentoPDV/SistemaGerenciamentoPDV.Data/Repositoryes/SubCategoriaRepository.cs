﻿using System;
using System.Collections.Generic;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System.Linq;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class SubCategoriaRepository : RepositoryBase<SubCategoria>, ISubCategoriaRepository
    {
        public IEnumerable<Categoria> GetAllCategoria()
        {
            return Db.Set<Categoria>().ToList();
        }
    }
}
