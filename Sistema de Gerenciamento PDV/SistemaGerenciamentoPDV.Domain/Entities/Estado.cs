﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("Estados")]
    public class Estado
    {
        [Column("EstadoId")]
        [Display(Name ="EstadoID")]
        public int EstadoId { get; set; }

        [Column("NomeEstado")]
        [Display(Name = "Estado")]
        public string NomeEstado { get; set; }

        [Column("UF")]
        [Display(Name = "UF")]
        public string Uf { get; set; }
    }
}
