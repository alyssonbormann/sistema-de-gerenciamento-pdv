﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("categoria")]
    public class Categoria
    {
        [Display(Name = "Código")]
        [Column("cat_cod")]
        [Key]
        public int Categoria_Codigo { get; set; }

        [Display(Name = "Categoria")]
        [Column("cat_nome")]
        [Required(ErrorMessage = "O nome da categoria é obrigatório", AllowEmptyStrings = false)]
        public string Categoria_Nome { get; set; }

        //[NotMapped]
        //public virtual ICollection<SubCategoria> SubCategorias { get; set; }
    }
}
