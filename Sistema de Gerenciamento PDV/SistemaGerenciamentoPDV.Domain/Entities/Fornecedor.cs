﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//TODO: FORMULARIO DE FORNECEDOR TODO CONCLUIDO
namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("Fornecedor")]
    public class Fornecedor
    {
        [Key]
        [Display(Name ="Código")]
        [Column("FornecedorId")]
        public int FornecedorId { get; set; }

        [Display(Name = "Nome")]
        [Column("Nome")]
        public string Nome { get; set; }

        [Display(Name = "Nome Fantasia")]
        [Column("NomeFantasia")]
        public string NomeFantasia { get; set; }

        [Display(Name = "Ativo")]
        [Column("Ativo")]
        public bool Ativo { get; set; }

        [Display(Name = "Tipo")]
        [Column("TipoPessoa")]
        public string TipoPessoa { get; set; }

        [Display(Name = "CPF/CNPJ")]
        [Column("CpfCnpj")]
        public string CpfCnpj { get; set; }

        [Display(Name = "RG/IE")]
        [Column("RgIE")]
        public string RgIE { get; set; }

        [Display(Name = "Telefone")]
        [Column("Telefone")]
        public string Telefone { get; set; }

        [Display(Name = "Celular")]
        [Column("Celular")]
        public string Celular { get; set; }

        [Display(Name = "E-MAIL")]
        [Column("Email")]
        public string Email { get; set; }

        [Display(Name = "Contato")]
        [Column("Contato")]
        public string Contato { get; set; }

        [Display(Name = "Cep")]
        [Column("Cep")]
        public string Cep { get; set; }

        [Display(Name = "Cidade")]
        [Column("CidadeId")]
        public int CidadeId { get; set; }

        [Display(Name = "Endereço")]
        [Column("Endereco")]
        public string Endereco { get; set; }

        [Display(Name = "Número")]
        [Column("Numero")]
        public string Numero { get; set; }

        [Display(Name = "Bairro")]
        [Column("Bairro")]
        public string Bairro { get; set; }

        [Display(Name = "Site Fornecedor")]
        public string SiteFornecedor { get; set; }

        [Display(Name ="Observação")]
        public string Observacao { get; set; }

        [Display(Name ="Data de Cadastro")]
        public DateTime DataCadastro { get; set; }
    }
}
