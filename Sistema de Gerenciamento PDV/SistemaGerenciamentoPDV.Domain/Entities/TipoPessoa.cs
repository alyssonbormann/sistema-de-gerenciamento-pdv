﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    public enum TipoPessoa
    {
        Fisica = 0,
        Juridica = 1
    }
}
