﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("parcelascompra")]
    public class ParcelasCompra
    {
        [Key]
        [Column("pco_cod")]
        [Display(Name ="Código")]
        public int ParcelasCompraId { get; set; }

        [Column("pco_valor")]
        [Display(Name ="Valor")]
        [Required(ErrorMessage ="Informe o valor")]
        public double Valor { get; set; }

        [Column("pco_datapagto")]
        [Display(Name = "Data do Pagamento")]
        [Required(ErrorMessage = "Informe a data do pagamento")]
        [DataType(DataType.Date)]
        public DateTime DataPagamento { get; set; }

        [Column("pco_datavecto")]
        [Display(Name = "Data do Vencimento")]
        [Required(ErrorMessage = "Informe a data do vencimento")]
        [DataType(DataType.Date)]
        public DateTime DataVencimento { get; set; }

        [Column("com_cod")]
        [Display(Name = "Compra")]
        [Required(ErrorMessage = "Informe a compra")]
        public int CompraId { get; set; }

    }
}
