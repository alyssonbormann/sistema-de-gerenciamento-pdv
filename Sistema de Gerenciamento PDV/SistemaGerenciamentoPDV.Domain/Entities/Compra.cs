﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//TODO: AULA 70 COMPRA
namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("compra")]
    public class Compra
    {
        [Key]
        [Display(Name = "Código")]
        [Column("com_cod")]
        public int CompraId { get; set; }

        [Display(Name = "Data")]
        [Column("com_data")]
        public DateTime CompraData { get; set; }

        [Display(Name = "Nº Nota Fiscal")]
        [Column("com_nfiscal")]
        public int NumeroNotalFiscal { get; set; }

        [Display(Name = "Total")]
        [Column("com_total")]
        public double Total { get; set; }

        [Display(Name = "Número Parcelas")]
        [Column("com_nparcelas")]
        public int NumeroParcelas { get; set; }

        [Display(Name = "Status")]
        [Column("com_status")]
        public StatusPedido Status { get; set; }//criei um enum

        [Display(Name = "FornecedorID")]
        [Column("for_cod")]
        public int FornecedorId { get; set; }

        [NotMapped]
        public Fornecedor Fornecedor { get; set; }

        [Display(Name = "Tipo Pagamento")]
        [Column("tpa_cod")]
        public int TipoPagamento { get; set; }
    }
    public enum StatusPedido
    {
        ativo = 0,
        inativo = 1,
        pedido = 2,
    }
}
