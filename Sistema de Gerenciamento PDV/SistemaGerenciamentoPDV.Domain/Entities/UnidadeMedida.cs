﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("undmedida")]
    public class UnidadeMedida
    {
        [Display(Name = "Código")]
        [Column("umed_cod")]
        [Key]
        public int UnidadeMedida_Codigo { get; set; }

        [Display(Name = "Unidade Medida")]
        [Column("umed_nome")]
        public string UnidadeMedida_Nome { get; set; }
    }
}
