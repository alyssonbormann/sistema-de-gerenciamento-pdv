﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("produto")]
    public class Produto
    {
        [Display(Name ="Código")]
        [Column("pro_cod")]
        [Key]
        public int Produto_Codigo { get; set; }

        [Display(Name = "Produto")]
        [Column("pro_nome")]
        public string Produto_Nome { get; set; }

        [Display(Name = "Descrição")]
        [Column("pro_descricao")]
        public string Produto_Descricao { get; set; }

        [Display(Name = "Foto")]
        [Column("pro_foto")]
        public byte[] Produto_Foto { get; set; }
        
        [Display(Name = "Valor Pago")]
        [Column("pro_valorpago")]
        public decimal Produto_ValorPago { get; set; }

        [Display(Name = "Valor Venda")]
        [Column("pro_valorvenda")]
        public decimal Produto_ValorVenda { get; set; }
        
        [Display(Name = "Quantidade")]
        [Column("pro_qtde")]
        public double Produto_Quantidade { get; set; }

        [Display(Name = "Uni. Medida")]
        [Column("umed_cod")]
        public int UnidadeMedida_Codigo { get; set; }

        [Display(Name = "Categoria")]
        [Column("cat_cod")]
        public int Categoria_Codigo { get; set; }

        [Display(Name = "SubCategoria")]
        [Column("scat_cod")]
        public int SubCategoria_Codigo { get; set; }

   
        public void CarregarImagem(string imgCaminho)
        {
            try
            {
                if (string.IsNullOrEmpty(imgCaminho))
                    return;
                //fornece propriedadese métodos de instância para criar, copiar,
                //excluir, mover, e abrir arquivos, e ajuda na criação de objetos FileStream
                FileInfo arqImagem = new FileInfo(imgCaminho);
                //Expõe um Stream ao redor de um arquivo de suporte
                //síncrono e assíncrono operações de leitura e gravar.
                FileStream fs = new FileStream(imgCaminho, FileMode.Open, FileAccess.Read, FileShare.Read);
                //aloca memória para o vetor
                this.Produto_Foto = new byte[Convert.ToInt32(arqImagem.Length)];
                //Lê um bloco de bytes do fluxo e grava osdados em um buffer fornecido.
                int iBytesRead = fs.Read(this.Produto_Foto, 0, Convert.ToInt32(arqImagem.Length));
                fs.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
    }
}
