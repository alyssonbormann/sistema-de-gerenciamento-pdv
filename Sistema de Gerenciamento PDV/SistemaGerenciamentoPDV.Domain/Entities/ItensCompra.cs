﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("itenscompra")]
    public class ItensCompra
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.None)]//Para nao gerar auto-incrimento
        [Column("itc_cod")]
        [Key]
        public int ItensCompraCodigo { get; set; }

        [Required(ErrorMessage ="Informe uma quantidade")]
        [Display(Name = "Quantidade")]
        [Column("itc_qtde")]
        public double Quantidade { get; set; }

        [Required(ErrorMessage ="Informe um valor")]
        [Display(Name = "Valor")]
        [Column("itc_valor")]
        public double Valor { get; set; }

        [Required(ErrorMessage = "Informe uma compra")]
        [Display(Name = "Compra")]
        [Column("com_cod")]
        public int CompraId { get; set; }

        [Required(ErrorMessage = "Informe um produto")]
        [Display(Name = "Produto")]
        [Column("pro_cod")]
        public int ProdutoId { get; set; }
    }
}
