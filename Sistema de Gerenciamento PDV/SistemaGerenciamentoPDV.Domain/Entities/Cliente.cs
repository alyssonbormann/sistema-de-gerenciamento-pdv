﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("cliente")]
    public class Cliente
    {
        [Display(Name ="Código")]
        [Column("cli_cod")]
        [Key]
        public int Cliente_Codigo { get; set; }

        [Display(Name = "Nome")]
        [Column("cli_nome")]
        public string Cliente_Nome { get; set; }

        [Display(Name = "CPF/CNPJ")]
        [Column("cli_cpfcnpj")]
        public string Cliente_Cpf_Cnpj { get; set; }

        [Display(Name = "Rasão Social")]
        [Column("cli_rsocial")]
        public string Cliente_RazaoSocial { get; set; }

        [Display(Name = "RG/IE")]
        [Column("cli_rgie")]
        public string Cliente_Rg_Ie { get; set; }

        [Display(Name = "Tipo")]
        [Column("cli_tipo")]
        public int Cliente_Tipo { get; set; }

        [Display(Name = "CEP")]
        [Column("cli_cep")]
        public string Cliente_Cep { get; set; }

        [Display(Name = "Endereço")]
        [Column("cli_endereco")]
        public string Cliente_Endereco { get; set; }

        [Display(Name = "Bairro")]
        [Column("cli_bairro")]
        public string Cliente_Bairro { get; set; }

        [Display(Name = "Telefone")]
        [Column("cli_fone")]
        public string Cliente_Fone { get; set; }

        [Display(Name = "Celular")]
        [Column("cli_cel")]
        public string Cliente_Celular { get; set; }

        [Display(Name = "E-Mail")]
        [Column("cli_email")]
        public string Cliente_Email { get; set; }

        [Display(Name = "Número")]
        [Column("cli_endnumero")]
        public string Cliente_NumeroEndereco { get; set; }

        [Display(Name = "Cidade")]
        [Column("cli_cidade")]
        public string Cliente_Cidade { get; set; }

        [Display(Name = "Estado")]
        [Column("cli_estado")]
        public string Cliente_Estado { get; set; }


    }
}
