﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Entities
{
    [Table("Cidades")]
    public class Cidade
    {
        [Display(Name ="CidadeID")]
        [Column("CidadeId")]
        public int CidadeId { get; set; }

        [Display(Name = "Código IBGE")]
        [Column("CodigoIBGE")]
        public int CodigoIBGE { get; set; }

        [Display(Name = "Nome")]
        [Column("Nome")]
        public string Nome { get; set; }

        [Column("UF")]
        [Display(Name = "UF")]
        public string Uf { get; set; }
    }
}
