﻿using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IProdutoRepository: IRepositoryBase<Produto>
    {
        IEnumerable<Categoria> GetAllCategoria();
        IEnumerable<SubCategoria> GetAllSubCategoria();
        IEnumerable<UnidadeMedida> GetAllUnidadeMedida();
        IEnumerable<SubCategoria> GetAllFindSubCategoriaCodigo(int codigo);
    }
}
