﻿using SistemaGerenciamentoPDV.Domain.Entities;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IUnidadeMedidaRepository: IRepositoryBase<UnidadeMedida>
    {
    }
}
