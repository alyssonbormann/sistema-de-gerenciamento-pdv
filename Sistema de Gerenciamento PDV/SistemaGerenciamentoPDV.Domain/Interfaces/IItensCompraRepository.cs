﻿using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IItensCompraRepository: IRepositoryBase<ItensCompra>
    {
        //Buscar todos os itens da compra, buscando pelo codigo da compra
        IEnumerable<ItensCompra> GetAllItensCompraCodCompra(int codigoCompra);

        ItensCompra CarregarItensCompra(int codigoItensCompra, int codigoCompra, int codigoProduto);

        void ExcluirPorCodigos(ItensCompra obj);

        //Medoto para criar o incremento a quantidade  da compra ao salvar os itens da compra
        void IncrementarItensCompra(int codigoProduto, double quantidade);

    }
}
