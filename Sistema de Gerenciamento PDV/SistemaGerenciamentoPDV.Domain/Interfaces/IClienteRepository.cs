﻿using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IClienteRepository: IRepositoryBase<Cliente>
    {
    }
}
