﻿using SistemaGerenciamentoPDV.Domain.Entities;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface IParcelasCompraRepository: IRepositoryBase<ParcelasCompra>
    {
    }
}
