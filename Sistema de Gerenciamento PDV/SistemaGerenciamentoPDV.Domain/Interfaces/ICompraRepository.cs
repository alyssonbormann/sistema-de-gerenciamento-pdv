﻿using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Domain.Interfaces
{
    public interface ICompraRepository: IRepositoryBase<Compra>
    {
        Fornecedor GetFindFornecedorCodigo(int codigo);

        Fornecedor GetFindFornecedorNome(string nome);

        IEnumerable<Compra> GetFindDataInicialFinal(DateTime inicial, DateTime final);

        Produto GetFindProdutoCodigo(int codigo);

        IEnumerable<TipoPagamento> GetAllTipoPagamento();
    }
}
