﻿using SistemaGerenciamentoPDV.Presentation.Formularios;
using System;
using System.Globalization;
using System.Windows.Forms;

namespace SistemaGerenciamentoPDV.Presentation
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var minhaCultura = new CultureInfo("pt-BR");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
