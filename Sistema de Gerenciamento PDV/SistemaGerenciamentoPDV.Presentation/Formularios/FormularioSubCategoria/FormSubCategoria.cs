﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Business.Service;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCategoria;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioSubCategoria
{
   
    public partial class FormSubCategoria : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private SubCategoria model;
        private SubCategoriaService _subCategoriaService;
        public int subCategoriaCodigo = 0;

        public FormSubCategoria()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
        }

        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
            cmbCategoria.SelectedIndex = -1;

        }
        
        private void FormSubCategoria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new SubCategoria();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.SubCategoria_Codigo = int.Parse(txtCodigo.Text);
                model.SubCategoria_Nome = txtNome.Text;
                model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);

                _subCategoriaService = new SubCategoriaService();

                if (model.SubCategoria_Codigo == 0)
                {
                    _subCategoriaService.Inserir(model);

                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                   
                    this.LimparCampos();
                }
                else
                {
                    _subCategoriaService.Alterar(model);

                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação",
                     MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();

                    this.LimparCampos();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void FormSubCategoria_Load(object sender, EventArgs e)
        {
            AtualizarComboBoxCategoria();

            FormSubCategoriaConsulta f = new FormSubCategoriaConsulta();
            if (subCategoriaCodigo != 0)
            {
                _subCategoriaService = new SubCategoriaService();
                model = new SubCategoria();
                model = _subCategoriaService.BuscarPorCodigo(subCategoriaCodigo);

                txtCodigo.Text = model.SubCategoria_Codigo.ToString();
                txtNome.Text = model.SubCategoria_Nome;
                cmbCategoria.SelectedValue = model.Categoria_Codigo;
            }
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new SubCategoria();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.SubCategoria_Codigo = int.Parse(txtCodigo.Text);
                model.SubCategoria_Nome = txtNome.Text;
                model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);

                _subCategoriaService = new SubCategoriaService();

                if (model.SubCategoria_Codigo == 0)
                {
                    _subCategoriaService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _subCategoriaService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString(); FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.","Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _subCategoriaService = new SubCategoriaService();
                        _subCategoriaService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                    }
                }
            }
            catch (Exception ex)
            {
                FlyoutMessageBox.Show(ex.Message, "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
            }
        }

        private void bbiFechar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FormSubCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new SubCategoria();
                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.SubCategoria_Codigo = int.Parse(txtCodigo.Text);
                    model.SubCategoria_Nome = txtNome.Text;
                    model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);

                    _subCategoriaService = new SubCategoriaService();

                    if (model.SubCategoria_Codigo == 0)
                    {
                        _subCategoriaService.Inserir(model);
                        FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                        this.Close();
                    }
                    else
                    {
                        _subCategoriaService.Alterar(model);
                        FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void sbBuscarCategoria_Click(object sender, EventArgs e)
        {
            FormCategoria frm = new FormCategoria();
            frm.ShowDialog();
            AtualizarComboBoxCategoria();
            frm.Dispose();
        }

        public void AtualizarComboBoxCategoria()
        {
            _subCategoriaService = new SubCategoriaService();
            cmbCategoria.DataSource = _subCategoriaService.BuscarCategorias().ToList();
            cmbCategoria.DisplayMember = "Categoria_Nome";
            cmbCategoria.ValueMember = "Categoria_Codigo";
        }
    }
}
