﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioConsultas
{
    public partial class FormConsultaProduto : DevExpress.XtraEditors.XtraForm
    {
        private ProdutoService _produtoService;
        public int codigo = 0;
        public FormConsultaProduto()
        {
            InitializeComponent();
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "Produto_Codigo");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "Produto_Codigo"));
            Close();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            this.RefreshGrid();
        }

        public void RefreshGrid()
        {
            _produtoService = new ProdutoService();
            gvDados.DataSource = _produtoService.BuscarTodos();
            gvDados.Refresh();
        }

        private void FormConsultaProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}