﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioConsultas
{
    public partial class FormConsultaFornecedor : DevExpress.XtraEditors.XtraForm
    {
        private FornecedorService _fornecedorService;
        public int codigo = 0;

        public FormConsultaFornecedor()
        {
            InitializeComponent();
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "FornecedorId");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "FornecedorId"));
            Close();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        public void RefreshGrid()
        {
            _fornecedorService = new FornecedorService();
            gvDados.DataSource = _fornecedorService.BuscarTodos();
            gvDados.Refresh();
        }

        private void FormConsultaFornecedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                this.Close();
            }
        }
    }
}