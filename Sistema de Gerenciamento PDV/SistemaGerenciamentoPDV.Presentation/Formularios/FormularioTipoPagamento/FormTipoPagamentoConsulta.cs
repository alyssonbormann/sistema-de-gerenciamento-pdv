﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioTipoPagamento
{
    public partial class FormTipoPagamentoConsulta : DevExpress.XtraEditors.XtraForm
    {
        private TipoPagamentoService _tipoPagamentoService;
        public int codigo = 0;

        public FormTipoPagamentoConsulta()
        {
            InitializeComponent();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            _tipoPagamentoService = new TipoPagamentoService();
            gvDados.DataSource = _tipoPagamentoService.BuscarTodos();
            gvDados.Refresh();
        }

        private void FormTipoPagamentoConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                    RefreshGrid();
                }
            }
        }

        private void windowsUIButtonPanel1_ButtonClick_1(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                FormTipoPagamento f = new FormTipoPagamento();
                f.ShowDialog();
                f.Dispose();
                RefreshGrid();
            }
            else if ((e.Button == windowsUIButtonPanel1.Buttons[3]))
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void gvDados_DoubleClick_1(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "TipoPagamento_Codigo");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "TipoPagamento_Codigo"));

            FormTipoPagamento f = new FormTipoPagamento();
            f.tipoPagamentoConsulta = codigo;
            f.ShowDialog();

            f.Dispose();
            RefreshGrid();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }
    }
}