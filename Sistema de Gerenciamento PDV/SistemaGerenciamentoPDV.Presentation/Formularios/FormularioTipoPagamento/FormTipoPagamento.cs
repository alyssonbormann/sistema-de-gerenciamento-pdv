﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using SistemaGerenciamentoPDV.Business.Service;
using SistemaGerenciamentoPDV.Domain.Entities;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioTipoPagamento
{
    public partial class FormTipoPagamento : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private TipoPagamentoService _tipoPagamentoService;
        private TipoPagamento model;
        public int tipoPagamentoConsulta = 0;

        public FormTipoPagamento()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
        }
        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new TipoPagamento();
                _tipoPagamentoService = new TipoPagamentoService();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.TipoPagamento_Codigo = int.Parse(txtCodigo.Text);
                model.TipoPagamento_Nome = txtNome.Text;

                if (model.TipoPagamento_Codigo == 0)
                {

                    _tipoPagamentoService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
                else
                {
                    _tipoPagamentoService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void FormTipoPagamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }

            if (e.KeyCode == Keys.Enter) //ENTER
            {
                try
                {
                    model = new TipoPagamento();
                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.TipoPagamento_Codigo = int.Parse(txtCodigo.Text);
                    model.TipoPagamento_Nome = txtNome.Text;

                    if (model.TipoPagamento_Codigo == 0)
                    {
                        _tipoPagamentoService.Inserir(model);
                        XtraMessageBox.Show("Cadastros efetuado com sucesso!!", "Informação");
                        this.LimparCampos();
                    }
                    else
                    {
                        _tipoPagamentoService.Alterar(model);
                        XtraMessageBox.Show("Cadastros alterado com sucesso!!", "Informação");
                        this.LimparCampos();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new TipoPagamento();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.TipoPagamento_Codigo = int.Parse(txtCodigo.Text);
                model.TipoPagamento_Nome = txtNome.Text;

                if (model.TipoPagamento_Codigo == 0)
                {
                    _tipoPagamentoService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _tipoPagamentoService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiConsultar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormTipoPagamentoConsulta f = new FormTipoPagamentoConsulta();
            f.ShowDialog();
            if (f.codigo != 0)
            {
                _tipoPagamentoService = new TipoPagamentoService();
                model = new TipoPagamento();
                model = _tipoPagamentoService.BuscarPorCodigo(f.codigo);
                txtCodigo.Text = Convert.ToString(model.TipoPagamento_Codigo);
                txtNome.Text = model.TipoPagamento_Nome;
            }
            else
            {
                this.LimparCampos();
            }
            f.Dispose();
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.", "Aviso",
                          MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _tipoPagamentoService = new TipoPagamentoService();
                        _tipoPagamentoService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                    }
                }
            }
            catch (Exception ex)
            {

                FlyoutMessageBox.Show(ex.Message, "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
            }
        }

        private void FormTipoPagamento_Load(object sender, EventArgs e)
        {
            FormTipoPagamentoConsulta f = new FormTipoPagamentoConsulta();
            if (tipoPagamentoConsulta != 0)
            {
                _tipoPagamentoService = new TipoPagamentoService();
                model = new TipoPagamento();
                model = _tipoPagamentoService.BuscarPorCodigo(tipoPagamentoConsulta);

                txtCodigo.Text = model.TipoPagamento_Codigo.ToString();
                txtNome.Text = model.TipoPagamento_Nome;
            }
        }
    }
}
