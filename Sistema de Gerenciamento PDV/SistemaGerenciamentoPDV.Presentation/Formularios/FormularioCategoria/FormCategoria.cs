﻿using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Windows.Forms;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCategoria
{
    public partial class FormCategoria : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private Categoria model;
        private CategoriaService _categoriaService;
        public int categoriaConsulta = 0;

        public FormCategoria()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
        }

        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Categoria();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.Categoria_Codigo = int.Parse(txtCodigo.Text);
                model.Categoria_Nome = txtNome.Text;

                _categoriaService = new CategoriaService();

                if (model.Categoria_Codigo == 0)
                {
                    _categoriaService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
                else
                {
                    _categoriaService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.Close();
                    this.LimparCampos();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void FormCategoria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Categoria();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.Categoria_Codigo = int.Parse(txtCodigo.Text);
                model.Categoria_Nome = txtNome.Text;

                _categoriaService = new CategoriaService();

                if (model.Categoria_Codigo == 0)
                {
                    _categoriaService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _categoriaService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.", "Aviso",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _categoriaService = new CategoriaService();
                        _categoriaService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        private void bbiFechar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FormCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new Categoria();
                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.Categoria_Codigo = int.Parse(txtCodigo.Text);
                    model.Categoria_Nome = txtNome.Text;

                    _categoriaService = new CategoriaService();

                    if (model.Categoria_Codigo == 0)
                    {
                        _categoriaService.Inserir(model);
                        FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                        this.Close();
                    }
                    else
                    {
                        _categoriaService.Alterar(model);
                        FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                        this.Close();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void FormCategoria_FormClosing(object sender, FormClosingEventArgs e)
        {
            FormCategoriaConsulta f = new FormCategoriaConsulta();
            f.RefreshGrid();
        }

        private void FormCategoria_Load(object sender, EventArgs e)
        {
            FormCategoriaConsulta f = new FormCategoriaConsulta();
            if (categoriaConsulta != 0)
            {
                _categoriaService = new CategoriaService();
                model = new Categoria();
                model = _categoriaService.BuscarPorCodigo(categoriaConsulta);

                txtCodigo.Text = model.Categoria_Codigo.ToString();
                txtNome.Text = model.Categoria_Nome;
            }
        }
    }
}
