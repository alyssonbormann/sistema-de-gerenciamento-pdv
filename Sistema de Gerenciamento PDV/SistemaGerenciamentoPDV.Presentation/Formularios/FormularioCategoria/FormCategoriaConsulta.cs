﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraBars;
using SistemaGerenciamentoPDV.Business.Service;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCategoria
{
    public partial class FormCategoriaConsulta : DevExpress.XtraEditors.XtraForm
    {
        private CategoriaService _categoriaService;
        public int codigo = 0;

        public FormCategoriaConsulta()
        {
            InitializeComponent();
            RefreshGrid();
        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, ButtonEventArgs e)
        {
            if (e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                FormCategoria f = new FormCategoria();
                f.ShowDialog();
                f.Dispose();
                RefreshGrid();
            }
            else if ((e.Button == windowsUIButtonPanel1.Buttons[3]))
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        public void RefreshGrid()
        {
            _categoriaService = new CategoriaService();
            gvDados.DataSource = _categoriaService.BuscarTodos();
            gvDados.Refresh();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "Categoria_Codigo");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "Categoria_Codigo"));

            FormCategoria f = new FormCategoria();
            f.categoriaConsulta = codigo;
            f.ShowDialog();

            f.Dispose();
            RefreshGrid();
        }

        private void FormCategoriaConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                    RefreshGrid();
                }
            }
        }
    }
}