﻿using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking2010;
using SistemaGerenciamentoPDV.Business.Service;
using System;
using System.Windows.Forms;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCliente
{
    public partial class FormClienteConsulta : DevExpress.XtraEditors.XtraForm
    {
        private ClienteService _clienteService;
        public int codigo = 0;

        public FormClienteConsulta()
        {
            InitializeComponent();
           // RefreshGrid();

        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, ButtonEventArgs e)
        {
            if (e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                FormCliente f = new FormCliente();
                f.ShowDialog();
                f.Dispose();
                RefreshGrid();
            }
            else if((e.Button == windowsUIButtonPanel1.Buttons[3]))
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
            
        }

        public void RefreshGrid()
        {
            _clienteService = new ClienteService();
            gvDados.DataSource = _clienteService.BuscarTodos();
            gvDados.Refresh();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "Cliente_Codigo");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "Cliente_Codigo"));

            FormCliente f = new FormCliente();
            f.codigoConsulta = codigo;
            f.ShowDialog();
           
            f.Dispose();
            RefreshGrid();
        }

        private void FormClienteConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }
    }
}