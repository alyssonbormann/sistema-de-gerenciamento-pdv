﻿using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Presentation.Ferramentas;
using System;
using System.Windows.Forms;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCliente
{
    public partial class FormCliente : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private Cliente model;
        private ClienteService _clienteService;
        public int codigoConsulta = 0;

        public FormCliente()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
            txtCpfCnpj.Mask = "000,000,000-00";
            txtRgIE.Mask = "000000";
        }

        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
            rbFisica.Checked = true;
            txtRazaoSocial.Text = string.Empty;
            txtCpfCnpj.Text = string.Empty;
            txtRgIE.Text = string.Empty;
            txtEstado.Text = string.Empty;
            txtCidade.Text = string.Empty;
            txtCep.Text = string.Empty;
            txtBairro.Text = string.Empty;
            txtEndereco.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtCelular.Text = string.Empty;
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Cliente();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }
                model.Cliente_Codigo = int.Parse(txtCodigo.Text);
                model.Cliente_Nome = txtNome.Text;
                if (rbFisica.Checked == true)
                {
                    model.Cliente_Tipo = 0; //Fisica
                }
                else
                {
                    model.Cliente_Tipo = 1; //Juridica
                }
                model.Cliente_RazaoSocial = txtRazaoSocial.Text;
                model.Cliente_Cpf_Cnpj = txtCpfCnpj.Text;
                model.Cliente_Rg_Ie = txtRgIE.Text;
                model.Cliente_Cep = txtCep.Text;
                model.Cliente_Endereco = txtEndereco.Text;
                model.Cliente_NumeroEndereco = txtNumero.Text;
                model.Cliente_Cidade = txtCidade.Text;
                model.Cliente_Estado = txtEstado.Text;
                model.Cliente_Bairro = txtBairro.Text;
                model.Cliente_Email = txtEmail.Text;
                model.Cliente_Fone = txtTelefone.Text;
                model.Cliente_Celular = txtCelular.Text;

                _clienteService = new ClienteService();

                if (model.Cliente_Codigo == 0)
                {
                    _clienteService.Incluir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
                else
                {
                    _clienteService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.Close();
                    this.LimparCampos();
                }

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void rbFisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFisica.Checked == true)
            {
                txtRazaoSocial.Enabled = false;
                txtRazaoSocial.Text = string.Empty;
                lblCpfCnpj.Text = "CPF";
                lblRgIE.Text = "RG";
                txtCpfCnpj.Text = string.Empty;
                txtRgIE.Text = string.Empty;
                txtCpfCnpj.Mask = "000,000,000-00";
                txtRgIE.Mask = "000000";
            }
            else
            {
                txtRazaoSocial.Enabled = true;
                lblCpfCnpj.Text = "CNPJ";
                lblRgIE.Text = "IE";
                txtCpfCnpj.Mask = "00,000,000/0000-00";
                txtRgIE.Mask = "";
                txtCpfCnpj.Text = string.Empty;
                txtRgIE.Text = string.Empty;
            }
        }

        private void FormCliente_Load(object sender, EventArgs e)
        {
            FormClienteConsulta f = new FormClienteConsulta();
            if (codigoConsulta != 0)
            {
                _clienteService = new ClienteService();
                model = new Cliente();
                model = _clienteService.BuscarPorCodigo(codigoConsulta);
                txtCodigo.Text = model.Cliente_Codigo.ToString();
                txtNome.Text = model.Cliente_Nome;
                if (model.Cliente_Tipo == 0)
                {
                    rbFisica.Checked = true;
                    rbJuridica.Checked = false;
                    txtRazaoSocial.Enabled = false;
                }
                else
                {
                    rbJuridica.Checked = true;
                    rbFisica.Checked = false;
                    txtRazaoSocial.Enabled = true;
                }
                txtRazaoSocial.Text = model.Cliente_RazaoSocial;
                txtCpfCnpj.Text = model.Cliente_Cpf_Cnpj;
                txtRgIE.Text = model.Cliente_Rg_Ie;
                txtEstado.Text = model.Cliente_Estado;
                txtCidade.Text = model.Cliente_Cidade;
                txtCep.Text = model.Cliente_Cep;
                txtEndereco.Text = model.Cliente_Endereco;
                txtNumero.Text = model.Cliente_NumeroEndereco;
                txtBairro.Text = model.Cliente_Bairro;
                txtEmail.Text = model.Cliente_Email;
                txtTelefone.Text = model.Cliente_Fone;
                txtCelular.Text = model.Cliente_Celular;
            }
            else
            {
                txtRazaoSocial.Enabled = false;
                lblCpfCnpj.Text = "CPF";
                lblRgIE.Text = "RG";
                txtCpfCnpj.Mask = "000,000,000-00";
                txtRgIE.Mask = "000000";
            }

        }

        private void txtCep_Leave(object sender, EventArgs e)
        {
            if (BuscarEndereco.VerificaCEP(txtCep.Text) == true)
            {
                txtEndereco.Text = BuscarEndereco.endereco;
                txtEstado.Text = BuscarEndereco.estado;
                txtCidade.Text = BuscarEndereco.cidade;
                txtBairro.Text = BuscarEndereco.bairro;
            }
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Cliente();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }
                model.Cliente_Codigo = int.Parse(txtCodigo.Text);
                model.Cliente_Nome = txtNome.Text;
                if (rbFisica.Checked == true)
                {
                    model.Cliente_Tipo = 0; //Fisica
                }
                else
                {
                    model.Cliente_Tipo = 1; //Juridica
                }
                model.Cliente_RazaoSocial = txtRazaoSocial.Text;
                model.Cliente_Cpf_Cnpj = txtCpfCnpj.Text;
                model.Cliente_Rg_Ie = txtRgIE.Text;
                model.Cliente_Cep = txtCep.Text;
                model.Cliente_Endereco = txtEndereco.Text;
                model.Cliente_NumeroEndereco = txtNumero.Text;
                model.Cliente_Bairro = txtBairro.Text;
                model.Cliente_Email = txtEmail.Text;
                model.Cliente_Cidade = txtCidade.Text;
                model.Cliente_Estado = txtEstado.Text;
                model.Cliente_Fone = txtTelefone.Text;
                model.Cliente_Celular = txtCelular.Text;

                _clienteService = new ClienteService();

                if (model.Cliente_Codigo == 0)
                {
                    _clienteService.Incluir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _clienteService.Alterar(model);

                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiFechar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.", "Aviso",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _clienteService = new ClienteService();
                        _clienteService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {

                FlyoutMessageBox.Show(ex.Message, "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2);
            }
        }

        private void FormCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void FormCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new Cliente();

                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }
                    model.Cliente_Codigo = int.Parse(txtCodigo.Text);
                    model.Cliente_Nome = txtNome.Text;
                    if (rbFisica.Checked == true)
                    {
                        model.Cliente_Tipo = 0; //Fisica
                    }
                    else
                    {
                        model.Cliente_Tipo = 1; //Juridica
                    }
                    model.Cliente_RazaoSocial = txtRazaoSocial.Text;
                    model.Cliente_Cpf_Cnpj = txtCpfCnpj.Text;
                    model.Cliente_Rg_Ie = txtRgIE.Text;
                    model.Cliente_Cep = txtCep.Text;
                    model.Cliente_Endereco = txtEndereco.Text;
                    model.Cliente_NumeroEndereco = txtNumero.Text;
                    model.Cliente_Cidade = txtCidade.Text;
                    model.Cliente_Estado = txtEstado.Text;
                    model.Cliente_Bairro = txtBairro.Text;
                    model.Cliente_Email = txtEmail.Text;
                    model.Cliente_Fone = txtTelefone.Text;
                    model.Cliente_Celular = txtCelular.Text;

                    _clienteService = new ClienteService();

                    if (model.Cliente_Codigo == 0)
                    {
                        _clienteService.Incluir(model);
                        FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                    }
                    else
                    {
                        _clienteService.Alterar(model);
                        FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                    }

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }
    }
}
