﻿namespace SistemaGerenciamentoPDV.Presentation.Formularios
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabbedView = new DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView(this.components);
            this.ribbonControl = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.skinRibbonGalleryBarItem = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barSubItemNavigation = new DevExpress.XtraBars.BarSubItem();
            this.employeesBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.customersBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCliente = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiProduto = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnItemCategoria = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSubCategoria = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnItemUnidadeMedida = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTipoPagamento = new DevExpress.XtraBars.BarButtonItem();
            this.skinRibbonGalleryBarItem1 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.skinRibbonGalleryBarItem2 = new DevExpress.XtraBars.SkinRibbonGalleryBarItem();
            this.barListItem1 = new DevExpress.XtraBars.BarListItem();
            this.barListItem2 = new DevExpress.XtraBars.BarListItem();
            this.barLinkContainerItem1 = new DevExpress.XtraBars.BarLinkContainerItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemColorPickEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.ribbonGalleryBarItem1 = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.bbiConfiguracaoBancoDados = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCalculadora = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExplorer = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNotepad = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.rpCadastros = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupNavigation = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.officeNavigationBar = new DevExpress.XtraBars.Navigation.OfficeNavigationBar();
            this.MdiManagerMain = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeNavigationBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MdiManagerMain)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonControl
            // 
            this.ribbonControl.ExpandCollapseItem.Id = 0;
            this.ribbonControl.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl.ExpandCollapseItem,
            this.skinRibbonGalleryBarItem,
            this.barSubItemNavigation,
            this.employeesBarButtonItem,
            this.customersBarButtonItem,
            this.bbiCliente,
            this.barButtonItem1,
            this.bbiProduto,
            this.barBtnItemCategoria,
            this.bbiSubCategoria,
            this.barBtnItemUnidadeMedida,
            this.bbiTipoPagamento,
            this.skinRibbonGalleryBarItem1,
            this.skinRibbonGalleryBarItem2,
            this.barListItem1,
            this.barListItem2,
            this.barLinkContainerItem1,
            this.barEditItem1,
            this.ribbonGalleryBarItem1,
            this.bbiConfiguracaoBancoDados,
            this.barButtonItem3,
            this.bbiCalculadora,
            this.bbiExplorer,
            this.bbiNotepad,
            this.barButtonItem2,
            this.barButtonItem4});
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.MaxItemId = 68;
            this.ribbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpCadastros,
            this.ribbonPage2,
            this.ribbonPage1,
            this.ribbonPage});
            this.ribbonControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorPickEdit1});
            this.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl.Size = new System.Drawing.Size(1362, 143);
            this.ribbonControl.StatusBar = this.ribbonStatusBar;
            this.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            // 
            // skinRibbonGalleryBarItem
            // 
            this.skinRibbonGalleryBarItem.Id = 14;
            this.skinRibbonGalleryBarItem.Name = "skinRibbonGalleryBarItem";
            // 
            // barSubItemNavigation
            // 
            this.barSubItemNavigation.Caption = "Navigation";
            this.barSubItemNavigation.Id = 15;
            this.barSubItemNavigation.ImageOptions.ImageUri.Uri = "NavigationBar";
            this.barSubItemNavigation.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.employeesBarButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.customersBarButtonItem)});
            this.barSubItemNavigation.Name = "barSubItemNavigation";
            // 
            // employeesBarButtonItem
            // 
            this.employeesBarButtonItem.Caption = "Employees";
            this.employeesBarButtonItem.Id = 44;
            this.employeesBarButtonItem.Name = "employeesBarButtonItem";
            // 
            // customersBarButtonItem
            // 
            this.customersBarButtonItem.Caption = "Customers";
            this.customersBarButtonItem.Id = 45;
            this.customersBarButtonItem.Name = "customersBarButtonItem";
            // 
            // bbiCliente
            // 
            this.bbiCliente.Caption = "Cliente";
            this.bbiCliente.Id = 46;
            this.bbiCliente.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.man_user;
            this.bbiCliente.Name = "bbiCliente";
            this.bbiCliente.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCliente_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Fornecedor";
            this.barButtonItem1.Id = 47;
            this.barButtonItem1.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.users;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiProduto
            // 
            this.bbiProduto.Caption = "Produto";
            this.bbiProduto.Id = 48;
            this.bbiProduto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiProduto.ImageOptions.Image")));
            this.bbiProduto.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiProduto.ImageOptions.LargeImage")));
            this.bbiProduto.Name = "bbiProduto";
            this.bbiProduto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiProduto_ItemClick);
            // 
            // barBtnItemCategoria
            // 
            this.barBtnItemCategoria.Caption = "Categoria";
            this.barBtnItemCategoria.Id = 49;
            this.barBtnItemCategoria.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnItemCategoria.ImageOptions.Image")));
            this.barBtnItemCategoria.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.category;
            this.barBtnItemCategoria.Name = "barBtnItemCategoria";
            this.barBtnItemCategoria.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnItemCategoria_ItemClick);
            // 
            // bbiSubCategoria
            // 
            this.bbiSubCategoria.Caption = "SubCategoria";
            this.bbiSubCategoria.Id = 50;
            this.bbiSubCategoria.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSubCategoria.ImageOptions.Image")));
            this.bbiSubCategoria.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.categories;
            this.bbiSubCategoria.Name = "bbiSubCategoria";
            this.bbiSubCategoria.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSubCategoria_ItemClick);
            // 
            // barBtnItemUnidadeMedida
            // 
            this.barBtnItemUnidadeMedida.Caption = "Unidade Médida";
            this.barBtnItemUnidadeMedida.Id = 51;
            this.barBtnItemUnidadeMedida.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barBtnItemUnidadeMedida.ImageOptions.Image")));
            this.barBtnItemUnidadeMedida.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.unidade;
            this.barBtnItemUnidadeMedida.Name = "barBtnItemUnidadeMedida";
            this.barBtnItemUnidadeMedida.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnItemUnidadeMedida_ItemClick);
            // 
            // bbiTipoPagamento
            // 
            this.bbiTipoPagamento.Caption = "Tipo de Pagamento";
            this.bbiTipoPagamento.Id = 52;
            this.bbiTipoPagamento.ImageOptions.LargeImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.credit_cards_payment;
            this.bbiTipoPagamento.Name = "bbiTipoPagamento";
            this.bbiTipoPagamento.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTipoPagamento_ItemClick);
            // 
            // skinRibbonGalleryBarItem1
            // 
            this.skinRibbonGalleryBarItem1.Caption = "skinRibbonGalleryBarItem1";
            this.skinRibbonGalleryBarItem1.Id = 53;
            this.skinRibbonGalleryBarItem1.Name = "skinRibbonGalleryBarItem1";
            // 
            // skinRibbonGalleryBarItem2
            // 
            this.skinRibbonGalleryBarItem2.Caption = "skinRibbonGalleryBarItem2";
            this.skinRibbonGalleryBarItem2.Id = 55;
            this.skinRibbonGalleryBarItem2.Name = "skinRibbonGalleryBarItem2";
            // 
            // barListItem1
            // 
            this.barListItem1.Caption = "barListItem1";
            this.barListItem1.Id = 56;
            this.barListItem1.Name = "barListItem1";
            // 
            // barListItem2
            // 
            this.barListItem2.Caption = "barListItem2";
            this.barListItem2.Id = 57;
            this.barListItem2.Name = "barListItem2";
            // 
            // barLinkContainerItem1
            // 
            this.barLinkContainerItem1.Caption = "barLinkContainerItem1";
            this.barLinkContainerItem1.Id = 58;
            this.barLinkContainerItem1.Name = "barLinkContainerItem1";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemColorPickEdit1;
            this.barEditItem1.Id = 59;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemColorPickEdit1
            // 
            this.repositoryItemColorPickEdit1.AutoHeight = false;
            this.repositoryItemColorPickEdit1.AutomaticColor = System.Drawing.Color.Black;
            this.repositoryItemColorPickEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit1.Name = "repositoryItemColorPickEdit1";
            // 
            // ribbonGalleryBarItem1
            // 
            this.ribbonGalleryBarItem1.Caption = "InplaceGallery1";
            this.ribbonGalleryBarItem1.Id = 60;
            this.ribbonGalleryBarItem1.Name = "ribbonGalleryBarItem1";
            // 
            // bbiConfiguracaoBancoDados
            // 
            this.bbiConfiguracaoBancoDados.Caption = "Configuração do Banco de Dados";
            this.bbiConfiguracaoBancoDados.Id = 61;
            this.bbiConfiguracaoBancoDados.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiConfiguracaoBancoDados.ImageOptions.LargeImage")));
            this.bbiConfiguracaoBancoDados.Name = "bbiConfiguracaoBancoDados";
            this.bbiConfiguracaoBancoDados.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiConfiguracaoBancoDados_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Backup do Bando de Dados";
            this.barButtonItem3.Id = 62;
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // bbiCalculadora
            // 
            this.bbiCalculadora.Caption = "Calculadora";
            this.bbiCalculadora.Id = 63;
            this.bbiCalculadora.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiCalculadora.ImageOptions.LargeImage")));
            this.bbiCalculadora.Name = "bbiCalculadora";
            this.bbiCalculadora.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCalculadora_ItemClick);
            // 
            // bbiExplorer
            // 
            this.bbiExplorer.Caption = "Explorer";
            this.bbiExplorer.Id = 64;
            this.bbiExplorer.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("bbiExplorer.ImageOptions.SvgImage")));
            this.bbiExplorer.Name = "bbiExplorer";
            this.bbiExplorer.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExplorer_ItemClick);
            // 
            // bbiNotepad
            // 
            this.bbiNotepad.Caption = "Bloco de Notas";
            this.bbiNotepad.Id = 65;
            this.bbiNotepad.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiNotepad.ImageOptions.LargeImage")));
            this.bbiNotepad.Name = "bbiNotepad";
            this.bbiNotepad.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNotepad_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Compras";
            this.barButtonItem2.Id = 66;
            this.barButtonItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.ImageOptions.Image")));
            this.barButtonItem2.ImageOptions.SvgImage = global::SistemaGerenciamentoPDV.Presentation.Properties.Resources.buy;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Vendas";
            this.barButtonItem4.Id = 67;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // rpCadastros
            // 
            this.rpCadastros.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.rpCadastros.Name = "rpCadastros";
            this.rpCadastros.Text = "Cadastros";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiCliente);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Pessoas";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiProduto);
            this.ribbonPageGroup2.ItemLinks.Add(this.barBtnItemCategoria);
            this.ribbonPageGroup2.ItemLinks.Add(this.bbiSubCategoria);
            this.ribbonPageGroup2.ItemLinks.Add(this.barBtnItemUnidadeMedida);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "Estoque";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.bbiTipoPagamento);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Outros";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup6});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Movimentação";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup5});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Ferrramentas";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.bbiConfiguracaoBancoDados);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Banco de dados";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.bbiCalculadora);
            this.ribbonPageGroup5.ItemLinks.Add(this.bbiExplorer);
            this.ribbonPageGroup5.ItemLinks.Add(this.bbiNotepad);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Utilitários";
            // 
            // ribbonPage
            // 
            this.ribbonPage.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupNavigation,
            this.ribbonPageGroup});
            this.ribbonPage.Name = "ribbonPage";
            this.ribbonPage.Text = "View";
            // 
            // ribbonPageGroupNavigation
            // 
            this.ribbonPageGroupNavigation.ItemLinks.Add(this.barSubItemNavigation);
            this.ribbonPageGroupNavigation.Name = "ribbonPageGroupNavigation";
            this.ribbonPageGroupNavigation.Text = "Module";
            // 
            // ribbonPageGroup
            // 
            this.ribbonPageGroup.AllowTextClipping = false;
            this.ribbonPageGroup.ItemLinks.Add(this.skinRibbonGalleryBarItem);
            this.ribbonPageGroup.Name = "ribbonPageGroup";
            this.ribbonPageGroup.ShowCaptionButton = false;
            this.ribbonPageGroup.Text = "Appearance";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 756);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbonControl;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1362, 31);
            // 
            // officeNavigationBar
            // 
            this.officeNavigationBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.officeNavigationBar.Location = new System.Drawing.Point(0, 711);
            this.officeNavigationBar.Name = "officeNavigationBar";
            this.officeNavigationBar.Size = new System.Drawing.Size(1362, 45);
            this.officeNavigationBar.TabIndex = 1;
            this.officeNavigationBar.Text = "officeNavigationBar";
            // 
            // MdiManagerMain
            // 
            this.MdiManagerMain.MdiParent = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 787);
            this.Controls.Add(this.officeNavigationBar);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbonControl);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Ribbon = this.ribbonControl;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Sistema de Gerenciamento PDV";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.tabbedView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeNavigationBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MdiManagerMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView tabbedView;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupNavigation;
        private DevExpress.XtraBars.BarSubItem barSubItemNavigation;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem;
        private DevExpress.XtraBars.Navigation.OfficeNavigationBar officeNavigationBar;
        private DevExpress.XtraBars.BarButtonItem employeesBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem customersBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem bbiCliente;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiProduto;
        private DevExpress.XtraBars.BarButtonItem barBtnItemCategoria;
        private DevExpress.XtraBars.BarButtonItem bbiSubCategoria;
        private DevExpress.XtraBars.BarButtonItem barBtnItemUnidadeMedida;
        private DevExpress.XtraBars.BarButtonItem bbiTipoPagamento;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpCadastros;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem1;
        private DevExpress.XtraBars.SkinRibbonGalleryBarItem skinRibbonGalleryBarItem2;
        private DevExpress.XtraBars.BarListItem barListItem1;
        private DevExpress.XtraBars.BarListItem barListItem2;
        private DevExpress.XtraBars.BarLinkContainerItem barLinkContainerItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit1;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItem1;
        private DevExpress.XtraBars.BarButtonItem bbiConfiguracaoBancoDados;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem bbiCalculadora;
        private DevExpress.XtraBars.BarButtonItem bbiExplorer;
        private DevExpress.XtraBars.BarButtonItem bbiNotepad;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager MdiManagerMain;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
    }
}