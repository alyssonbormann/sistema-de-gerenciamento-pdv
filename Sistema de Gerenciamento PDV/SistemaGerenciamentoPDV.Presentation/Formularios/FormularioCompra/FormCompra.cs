﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioConsultas;
using SistemaGerenciamentoPDV.Business.Service;
using SistemaGerenciamentoPDV.Domain.Entities;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCompra
{
    public partial class FormCompra : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt = new DataTable();
        DataTable dtParcela = new DataTable();

        private ItensCompra itensCompra;
        private Compra model;

        private ItensCompraService _itensCompraService;
        private CompraService _compraService;

        private double TotalCompra = 0;

        public FormCompra()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
            txtDataCompra.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            txtDataInicalPagamento.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            txtTotalCompra.Enabled = false;
            txtTotal.Enabled = false;
            pnlDadosPagamento.Enabled = false;
            lblNomeFornecedor.Text = " ";
            lblNomeProduto.Text = " ";
            cmbNumeroParcelas.EditValue = 1;
            
        }

        private void txtFornecedorCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                _compraService = new CompraService();
                Fornecedor fornecedor = _compraService.BuscarPorCodigoFornecedor(Convert.ToInt32(txtFornecedorCodigo.Text));
                lblNomeFornecedor.Text = fornecedor.Nome;
            }
            catch
            {
                txtFornecedorCodigo.Text = string.Empty;
                lblNomeFornecedor.Text = "Fornedor não localizado.";
                txtFornecedorCodigo.Focus();
                FlyoutMessageBox.Show("Informe o código do fornecedor ou clique em localizar", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
            }
        }

        private void txtProdutoCodigo_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtProdutoCodigo.Text != "0")
                {
                    _compraService = new CompraService();
                    Produto produto = _compraService.BuscarPorCodigoProduto(Convert.ToInt32(txtProdutoCodigo.Text));
                    lblNomeProduto.Text = produto.Produto_Nome;
                    txtQuantidade.Text = "1";
                    txtValorUnitario.Text = produto.Produto_ValorPago.ToString("n2"); 
                }
            }
            catch
            {

                txtProdutoCodigo.Text = string.Empty;
                lblNomeProduto.Text = "Produto não localizado.";
                
                FlyoutMessageBox.Show("Informe o código do produto ou clique em localizar", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                txtProdutoCodigo.Focus();
            }
        }

        private void txtFornecedorCodigo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FormConsultaFornecedor f = new FormConsultaFornecedor();
            f.ShowDialog();
            if (f.codigo != 0)
            {
                txtFornecedorCodigo.Text = f.codigo.ToString();
                txtFornecedorCodigo_Leave(sender, e);
            }
        }

        private void txtProdutoCodigo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            FormConsultaProduto f = new FormConsultaProduto();
            f.ShowDialog();
            if (f.codigo != 0)
            {
                txtProdutoCodigo.Text = f.codigo.ToString();
                txtProdutoCodigo_Leave(sender, e);
            }
        }

        private void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNumeroNotaFiscal.Text = string.Empty;
            txtDataCompra.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            txtDataInicalPagamento.Text = DateTime.Today.Date.ToString("dd/MM/yyyy");
            txtFornecedorCodigo.Text = string.Empty;
            lblNomeFornecedor.Text = " ";
            lblNomeProduto.Text = " ";
            txtQuantidade.Text = string.Empty;
            txtValorUnitario.Text = string.Empty;
            cmbNumeroParcelas.EditValue = 1;
            cmbTipoPagamento.EditValue = -1;
            txtTotal.Text = string.Empty;
            txtTotalCompra.Text = string.Empty;
            gvItensCompra.DataSource = null;
            gvDadosParcela.DataSource = null;
        }

        private void FormCompra_Load(object sender, EventArgs e)
        {
            //Criação dos nomes dos campos na grid
            dt = new DataTable();
            dt.Columns.Add("Codigo");
            dt.Columns.Add("Nome");
            dt.Columns.Add("Quantidade");
            dt.Columns.Add("Valor Unitário");
            dt.Columns.Add("Valor Total");
            gvItensCompra.DataSource = dt;

            GridDadosParcela();
            //Carregar o comboBox TipoPagamento
            CarregarTipoPagamento();
        }

        void CarregarTipoPagamento()
        {
            _compraService = new CompraService();

            cmbTipoPagamento.Properties.DataSource = _compraService.CarregarTipoPagamento();
            cmbTipoPagamento.Properties.DisplayMember = "TipoPagamento_Nome";
            cmbTipoPagamento.Properties.ValueMember = "TipoPagamento_Codigo";
            cmbTipoPagamento.Properties.PopulateColumns();
            cmbTipoPagamento.Properties.Columns[0].Visible = false;

        }

        private void gvItensCompra_DoubleClick(object sender, EventArgs e)
        {
            //Verificar se tem itens no grid
            if (gridView1.FocusedRowHandle >= 0)
            {
                var rowHandle = gridView1.FocusedRowHandle;
                //btnBuscarProduto.Text = gridView1.GetRowCellValue(rowHandle, "Codigo").ToString() ; Existe esse jeito tmbm
                txtProdutoCodigo.Text = gridView1.GetRowCellValue(rowHandle, gridView1.Columns[0]).ToString();
                lblNomeProduto.Text = gridView1.GetRowCellValue(rowHandle, gridView1.Columns[1]).ToString();
                txtQuantidade.Text = gridView1.GetRowCellValue(rowHandle, gridView1.Columns[2]).ToString();
                txtValorUnitario.Text = gridView1.GetRowCellValue(rowHandle, gridView1.Columns[3]).ToString();

                double valor = Convert.ToDouble(gridView1.GetRowCellValue(rowHandle, gridView1.Columns[4]));
                this.TotalCompra = this.TotalCompra - valor;

                gridView1.DeleteRow(rowHandle);//Remover a linha selecionada

                txtTotal.Text = this.TotalCompra.ToString("n2");
            }
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            if ((txtProdutoCodigo.Text != "") && (txtQuantidade.Text != "") && (txtValorUnitario.Text.ToString() != ""))
            {
                double totalLocal = Convert.ToDouble(txtQuantidade.Text) * Convert.ToDouble(txtValorUnitario.Text.Replace(".", ","));
                this.TotalCompra = this.TotalCompra + totalLocal;

                //adicionar o campos do produto na grid
                
                DataRow dr = dt.NewRow();
                dr[0] = txtProdutoCodigo.Text;
                dr[1] = lblNomeProduto.Text;
                dr[2] = txtQuantidade.Text;
                dr[3] = Double.Parse(txtValorUnitario.Text.Replace(".", ",")).ToString("n2");
                dr[4] = Double.Parse(totalLocal.ToString("n2"));
                dt.Rows.Add(dr);
                gvItensCompra.DataSource = dt;

                txtProdutoCodigo.Text = "";
                lblNomeProduto.Text = "Informe o código do produto.";
                txtQuantidade.Text = "0";
                txtValorUnitario.Text = "0";
                txtTotal.Text = this.TotalCompra.ToString("n2");

            }
            else
            {
                FlyoutMessageBox.Show("Informe o código do produto ou clique em localizar", "ATENÇÃO", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                txtProdutoCodigo.Focus();
            }
        }

        void GridDadosParcela()
        {
            dtParcela = new DataTable();
            dtParcela.Columns.Add("Parcela");
            dtParcela.Columns.Add("Vlr. da Parcela");
            dtParcela.Columns.Add("Data Vencimento");
            gvDadosParcela.DataSource = dtParcela;
        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            //TODO: FAZER O ARRENDAMENTO DAS PARCELAS
            if (e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                //TODO:criar a linha para excluir todos os itens do grid das parcelas
                pnlMovimentacao.Enabled = false;
                pnlDadosPagamento.Enabled = true;

                int parcelas = Convert.ToInt32(cmbNumeroParcelas.Text);
                double totalLocal = this.TotalCompra;
                double valor = totalLocal / parcelas;
                DateTime data = new DateTime();
                data = Convert.ToDateTime(txtDataInicalPagamento.Text);
                txtTotalCompra.Text = TotalCompra.ToString();
                for (int i = 1; i <= parcelas; i++)
                {
                    DataRow drp = dtParcela.NewRow();
                    drp[0] = i.ToString();
                    drp[1] = valor.ToString();
                    drp[2] = data.Date.ToString();

                    dtParcela.Rows.Add(drp);
                    gvDadosParcela.DataSource = drp;

                    //Definir o mes para o proximo pagamento da parcela
                    if (data.Month != 12)
                    {
                        data = new DateTime(data.Year, data.Month + 1, data.Day);
                    }
                    else
                    {
                        data = new DateTime(data.Year + 1, 1, data.Day);
                    }
                }
            }
            
        }

        private void btnsDadosPagamento_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button == btnsDadosPagamento.Buttons[0])
            {
                model = new Compra();
                itensCompra = new ItensCompra();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }
                model.CompraData = Convert.ToDateTime(txtDataCompra.Text);
                model.NumeroNotalFiscal = Convert.ToInt32(txtNumeroNotaFiscal.Text);
                model.NumeroParcelas = Convert.ToInt32(cmbNumeroParcelas.Text);
                model.Status = 0;//ativo
                model.Total = this.TotalCompra;
                model.FornecedorId = Convert.ToInt32(txtFornecedorCodigo.Text);
                model.TipoPagamento = Convert.ToInt32(cmbTipoPagamento.EditValue);

                itensCompra = new ItensCompra();
                _itensCompraService = new ItensCompraService();
                var rowHandle = gridView1.FocusedRowHandle;

                if (model.CompraId == 0)
                {
                    _compraService.Incluir(model);
                    //Cadastrar os itens da compra
                    for (int i = 0; i < gridView1.RowCount; i++)
                    {
                        itensCompra.ItensCompraCodigo = i + 1;
                        itensCompra.CompraId = model.CompraId;
                        rowHandle = i;//incremento para mudar de linha na grid
                        itensCompra.ProdutoId = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, gridView1.Columns[0]));
                        itensCompra.Quantidade = Convert.ToDouble(gridView1.GetRowCellValue(rowHandle, gridView1.Columns[2]));
                        itensCompra.Valor = Convert.ToDouble(gridView1.GetRowCellValue(rowHandle, gridView1.Columns[3]));

                        _itensCompraService.Incluir(itensCompra);
                        _itensCompraService.IncrementarItensCompra(itensCompra.ProdutoId, itensCompra.Quantidade);
                        //TODO: alterar a quantidade de produtos comprados na tela de produtos
                        //"89"trigger
                    }
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();

                    pnlDadosPagamento.Enabled = false;
                    pnlMovimentacao.Enabled = true;
                }
                else
                {
                    _compraService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.Close();
                    this.LimparCampos();

                    //TODO: HABILITAR E DESABILITAR OS PANEL
                }
            }
            if (e.Button == btnsDadosPagamento.Buttons[1])
            {
                this.LimparCampos();
            }
        }
    }
}