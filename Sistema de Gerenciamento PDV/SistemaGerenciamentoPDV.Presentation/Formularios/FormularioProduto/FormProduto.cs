﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioProduto
{
    public partial class FormProduto : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private Produto model;
        private ProdutoService _produtoService;
        private string foto = string.Empty;
        private byte[] fotoUpdate;
        public int codigoProduto = 0;

        public FormProduto()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
        }

        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtDescricao.Text = string.Empty;
            txtValorPago.Text = string.Empty;
            txtValorVenda.Text = string.Empty;
            txtQuantidade.Text = string.Empty;
            cmbUnidadeMedida.SelectedIndex = -1;
            cmbCategoria.SelectedIndex = -1;
            cmbSubCategoria.SelectedIndex = -1;
            pbFoto.Image = null;
            foto = string.Empty;
            fotoUpdate = null;
        }

        private void FormProduto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void FormProduto_Load(object sender, EventArgs e)
        {
            _produtoService = new ProdutoService();

            //ComboBox Unidade Medida
            cmbUnidadeMedida.DataSource = _produtoService.CarregarUnidadeMedida();
            cmbUnidadeMedida.DisplayMember = "UnidadeMedida_Nome";
            cmbUnidadeMedida.ValueMember = "UnidadeMedida_Codigo";

            //ComboBox Categoria
            cmbCategoria.DataSource = _produtoService.CarregarCategorias();
            cmbCategoria.DisplayMember = "Categoria_Nome";
            cmbCategoria.ValueMember = "Categoria_Codigo";


            //ComboBox SubCategoria
            cmbSubCategoria.DataSource = _produtoService.CarregarSubCategoriaCodigo(Convert.ToInt32(cmbCategoria.SelectedValue)).ToList();
            cmbSubCategoria.DisplayMember = "SubCategoria_Nome";
            cmbSubCategoria.ValueMember = "SubCategoria_Codigo";

            if (codigoProduto != 0)
            {
                model = new Produto();
                model = _produtoService.BuscarPorCodigo(codigoProduto);

                txtCodigo.Text = model.Produto_Codigo.ToString();
                txtCodigo.Text = model.Produto_Codigo.ToString();
                txtNome.Text = model.Produto_Nome;
                txtDescricao.Text = model.Produto_Descricao;
                txtValorPago.Text = Convert.ToString(Math.Round(model.Produto_ValorPago, 2));
                txtValorVenda.Text = Convert.ToString(Math.Round(model.Produto_ValorVenda, 2));
                txtQuantidade.Text = Convert.ToString(model.Produto_Quantidade);
                cmbUnidadeMedida.SelectedValue = model.UnidadeMedida_Codigo;
                cmbCategoria.SelectedValue = model.Categoria_Codigo;
                cmbSubCategoria.SelectedValue = model.SubCategoria_Codigo;
                fotoUpdate = model.Produto_Foto;
                try
                {
                    MemoryStream ms = new MemoryStream(model.Produto_Foto);
                    pbFoto.Image = Image.FromStream(ms);
                }
                catch
                {

                }


            }

        }

        private void FormProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new Produto();
                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.Produto_Codigo = int.Parse(txtCodigo.Text);
                    model.Produto_Nome = txtNome.Text;
                    model.Produto_Descricao = txtDescricao.Text;
                    model.Produto_ValorPago = Convert.ToDecimal(txtValorPago.Text.Replace(".", ""));
                    model.Produto_ValorVenda = Convert.ToDecimal(txtValorVenda.Text.Replace(".", ""));
                    model.Produto_Quantidade = float.Parse(txtQuantidade.Text.Replace(".", ""));
                    model.UnidadeMedida_Codigo = Convert.ToInt32(cmbUnidadeMedida.SelectedValue);
                    model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);
                    model.SubCategoria_Codigo = Convert.ToInt32(cmbSubCategoria.SelectedValue);

                    _produtoService = new ProdutoService();

                    if (model.Produto_Codigo == 0)
                    {
                        model.CarregarImagem(this.foto);
                        _produtoService.Inserir(model);
                        FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                    }
                    else
                    {
                        if (this.foto == string.Empty)
                        {
                            model.Produto_Foto = fotoUpdate;
                        }
                        else
                        {
                            model.CarregarImagem(this.foto);
                        }
                        _produtoService.Alterar(model);
                        FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                    }


                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void bbiFechar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                     MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Produto();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.Produto_Codigo = int.Parse(txtCodigo.Text);
                model.Produto_Nome = txtNome.Text;
                model.Produto_Descricao = txtDescricao.Text;
                model.Produto_ValorPago = Convert.ToDecimal(txtValorPago.Text.Replace(".", ""));
                model.Produto_ValorVenda = Convert.ToDecimal(txtValorVenda.Text.Replace(".", ""));
                model.Produto_Quantidade = float.Parse(txtQuantidade.Text.Replace(".", ""));
                model.UnidadeMedida_Codigo = Convert.ToInt32(cmbUnidadeMedida.SelectedValue);
                model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);
                model.SubCategoria_Codigo = Convert.ToInt32(cmbSubCategoria.SelectedValue);

                _produtoService = new ProdutoService();

                if (model.Produto_Codigo == 0)
                {
                    model.CarregarImagem(this.foto);
                    _produtoService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
                else
                {
                    if (this.foto == string.Empty)
                    {
                        model.Produto_Foto = fotoUpdate;
                    }
                    else
                    {
                        model.CarregarImagem(this.foto);
                    }
                    _produtoService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }


            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void cmbCategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cmbSubCategoria.Text = string.Empty;
                cmbSubCategoria.DataSource = _produtoService.CarregarSubCategoriaCodigo(Convert.ToInt32(cmbCategoria.SelectedValue)).ToList();
                cmbSubCategoria.DisplayMember = "SubCategoria_Nome";
                cmbSubCategoria.ValueMember = "SubCategoria_Codigo";
            }
            catch { }

        }

        private void btnCarregarFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.ShowDialog();
            if (!string.IsNullOrEmpty(openFile.FileName))
            {
                this.foto = openFile.FileName;
                pbFoto.Load(this.foto);
            }
        }

        private void btnRemoverFoto_Click(object sender, EventArgs e)
        {
            this.foto = string.Empty;
            pbFoto.Image = null;
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Produto();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.Produto_Codigo = int.Parse(txtCodigo.Text);
                model.Produto_Nome = txtNome.Text;
                model.Produto_Descricao = txtDescricao.Text;
                model.CarregarImagem(this.foto);
                model.Produto_ValorPago = Convert.ToDecimal(txtValorPago.Text.Replace(".", ""));
                model.Produto_ValorVenda = Convert.ToDecimal(txtValorVenda.Text.Replace(".", ""));
                model.Produto_Quantidade = float.Parse(txtQuantidade.Text.Replace(".", ""));
                model.UnidadeMedida_Codigo = Convert.ToInt32(cmbUnidadeMedida.SelectedValue);
                model.Categoria_Codigo = Convert.ToInt32(cmbCategoria.SelectedValue);
                model.SubCategoria_Codigo = Convert.ToInt32(cmbSubCategoria.SelectedValue);

                _produtoService = new ProdutoService();

                if (model.Produto_Codigo == 0)
                {
                    _produtoService.Inserir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    if (this.foto == string.Empty)
                    {
                        model.Produto_Foto = fotoUpdate;
                    }
                    else
                    {
                        model.CarregarImagem(this.foto);
                    }
                    _produtoService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.", "Aviso",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _produtoService = new ProdutoService();
                        _produtoService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
