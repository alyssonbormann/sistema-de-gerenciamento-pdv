﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioProduto
{
    public partial class FormProdutoConsulta : DevExpress.XtraEditors.XtraForm
    {
        private ProdutoService _produtoService;
        public int codigo = 0;

        public FormProdutoConsulta()
        {
            InitializeComponent();
        }

        private void RefreshGrid()
        {
            _produtoService = new ProdutoService();
            gvDados.DataSource = _produtoService.BuscarProdutoPorNome("");
            gvDados.Refresh();
        } 

        private void FormProdutoConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                    RefreshGrid();
                }
            }
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "Produto_Codigo");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "Produto_Codigo"));

            FormProduto f = new FormProduto();
            f.codigoProduto = codigo;
            f.ShowDialog();

            f.Dispose();
            RefreshGrid();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if (e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                FormProduto f = new FormProduto();
                f.ShowDialog();
                f.Dispose();
                RefreshGrid();
            }
            else if ((e.Button == windowsUIButtonPanel1.Buttons[3]))
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                       MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }
    }
}