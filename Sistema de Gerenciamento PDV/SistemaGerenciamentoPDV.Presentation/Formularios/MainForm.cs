﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using SistemaGerenciamentoPDV.Presentation.Formularios.UnidadeMedida;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCategoria;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioSubCategoria;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioProduto;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFerramentas;
using DevExpress.XtraEditors.Popup;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioTipoPagamento;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCliente;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFornecedor;
using SistemaGerenciamentoPDV.Presentation.Formularios.FormularioCompra;
using DevExpress.XtraBars.Ribbon;

namespace SistemaGerenciamentoPDV.Presentation.Formularios
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public MainForm()
        {
            InitializeComponent();

            UserLookAndFeel defaultLF = UserLookAndFeel.Default;
            defaultLF.UseWindowsXPTheme = false;
            defaultLF.Style = LookAndFeelStyle.Skin;
            defaultLF.SkinName = "Office 2016 Colorful";
            
        }
     


        private void barBtnItemUnidadeMedida_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormUnidadeMedidaConsulta f = new FormUnidadeMedidaConsulta();
            f.MdiParent = this;
            f.Show();
        }

        private void barBtnItemCategoria_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormCategoriaConsulta f = new FormCategoriaConsulta();
            f.MdiParent = this;
            f.Show();
        }
        private void bbiSubCategoria_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormSubCategoriaConsulta f = new FormSubCategoriaConsulta();
            f.MdiParent = this;
            f.Show();
        }

        private void bbiProduto_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormProdutoConsulta f = new FormProdutoConsulta();
            f.MdiParent = this;
            f.Show();
        }

        private void bbiConfiguracaoBancoDados_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormConfiguracaoBancoDados f = new FormConfiguracaoBancoDados();
            f.ShowDialog();
            f.Dispose();
        }

        private void bbiCalculadora_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("calc");
        }

        private void bbiExplorer_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer");
        }

        private void bbiNotepad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("notepad");
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormBackupBancoDados f = new FormBackupBancoDados();
            f.ShowDialog();
            f.Dispose();
        }

        private void bbiTipoPagamento_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormTipoPagamentoConsulta f = new FormTipoPagamentoConsulta();
            f.MdiParent = this;
            f.Show();
        }

        private void bbiCliente_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormClienteConsulta f = new FormClienteConsulta();
            f.MdiParent = this;
            f.Show();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormFornecedor f = new FormFornecedor();
            IsMdiContainer = true;
            //this.ribbonControl.MdiMergeStyle = RibbonMdiMergeStyle.OnlyWhenMaximized;

            f.MdiParent = this;
            f.Show();
        }
        
        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormCompra f = new FormCompra();
            
            f.MdiParent = this;
            f.Show();
        }
    }
}