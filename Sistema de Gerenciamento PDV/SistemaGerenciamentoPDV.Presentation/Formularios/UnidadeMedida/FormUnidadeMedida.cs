﻿using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using System;
using System.Windows.Forms;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.UnidadeMedida
{
    public partial class FormUnidadeMedida : DevExpress.XtraBars.Ribbon.RibbonForm
    {

        SistemaGerenciamentoPDV.Domain.Entities.UnidadeMedida model;
        UnidadeMedidaService _unidadeMedidaService;
        public int unidadeMedidaConsulta = 0;

        public FormUnidadeMedida()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
        }

        void LimparCampos()
        {
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
        }

        private void bbiConsulta_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FormUnidadeMedidaConsulta frm = new FormUnidadeMedidaConsulta();
            frm.ShowDialog();
            if (frm.codigo != 0)
            {
                _unidadeMedidaService = new UnidadeMedidaService();
                model = new Domain.Entities.UnidadeMedida();
                model = _unidadeMedidaService.BuscarPorCodigo(frm.codigo);
                txtCodigo.Text = model.UnidadeMedida_Codigo.ToString();
                txtNome.Text = model.UnidadeMedida_Nome;
            }
            else
            {
                this.LimparCampos();
            }
            frm.Dispose();
        }
      
        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Domain.Entities.UnidadeMedida();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.UnidadeMedida_Codigo = int.Parse(txtCodigo.Text);
                model.UnidadeMedida_Nome = txtNome.Text;

                _unidadeMedidaService = new UnidadeMedidaService();

                if (model.UnidadeMedida_Codigo == 0)
                {
                    _unidadeMedidaService.Inserir(model);
                    XtraMessageBox.Show("Cadastros efetuado com sucesso!!","Informação");
                    this.LimparCampos();
                }
                else
                {
                    _unidadeMedidaService.Alterar(model);
                    XtraMessageBox.Show("Cadastros alterado com sucesso!!", "Informação");
                    this.LimparCampos();
                }

            }
            catch (Exception ex) 
            {
                XtraMessageBox.Show(ex.Message,"Aviso");
            }
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                model = new Domain.Entities.UnidadeMedida();

                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.UnidadeMedida_Codigo = int.Parse(txtCodigo.Text);
                model.UnidadeMedida_Nome = txtNome.Text;

                _unidadeMedidaService = new UnidadeMedidaService();

                if (model.UnidadeMedida_Codigo == 0)
                {
                    _unidadeMedidaService.Inserir(model);
                    XtraMessageBox.Show("Cadastros efetuado com sucesso!!", "Informação");
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _unidadeMedidaService.Alterar(model);
                    XtraMessageBox.Show("Cadastros alterado com sucesso!!", "Informação");
                    this.LimparCampos();
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LimparCampos();
        }

        private void bbiDeletar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    DialogResult d = XtraMessageBox.Show("Deseja realmente deletar o registro.", "Aviso", MessageBoxButtons.YesNo);
                    if (d.ToString() == "Yes")
                    {
                        _unidadeMedidaService = new UnidadeMedidaService();
                        _unidadeMedidaService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                    } 
                }
                else
                {
                    XtraMessageBox.Show("Nenhum registro selecionado para deleção.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message,"Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bbiFechar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult d = XtraMessageBox.Show("Deseja realmente fechar o formulário?", "Atenção", MessageBoxButtons.YesNo);
            if (d.ToString() == "Yes")
            {
                this.Close();
            }
        }

        private void FormUnidadeMedida_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void FormUnidadeMedida_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new Domain.Entities.UnidadeMedida();

                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.UnidadeMedida_Codigo = int.Parse(txtCodigo.Text);
                    model.UnidadeMedida_Nome = txtNome.Text;

                    _unidadeMedidaService = new UnidadeMedidaService();

                    if (model.UnidadeMedida_Codigo == 0)
                    {
                        _unidadeMedidaService.Inserir(model);
                        XtraMessageBox.Show("Cadastros efetuado com sucesso!!", "Informação");
                        this.LimparCampos();
                        this.Close();
                    }
                    else
                    {
                        _unidadeMedidaService.Alterar(model);
                        XtraMessageBox.Show("Cadastros alterado com sucesso!!", "Informação");
                        this.LimparCampos();
                        this.Close();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void FormUnidadeMedida_Load(object sender, EventArgs e)
        {
            FormUnidadeMedidaConsulta f = new FormUnidadeMedidaConsulta();
            if (unidadeMedidaConsulta != 0)
            {
                _unidadeMedidaService = new UnidadeMedidaService();
                model = new SistemaGerenciamentoPDV.Domain.Entities.UnidadeMedida();
                model = _unidadeMedidaService.BuscarPorCodigo(unidadeMedidaConsulta);

                txtCodigo.Text = model.UnidadeMedida_Codigo.ToString();
                txtNome.Text = model.UnidadeMedida_Nome;
            }
        }
    }
}
