﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFornecedor
{
    public partial class FormFornecedor : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private Fornecedor model;
        private FornecedorService _fornecedorService;
        public int codigoConsultaFornecedor = 0;

        public FormFornecedor()
        {
            InitializeComponent();
            txtCodigo.Enabled = false;
            txtDataCadastro.Enabled = false;
            txtDataCadastro.Text = DateTime.Now.Date.ToString();
            txtCpfCnpj.Mask = "000,000,000-00";
            txtRgIE.Mask = "000000"; 
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
          
            try
            {
                model = new Fornecedor();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.FornecedorId = int.Parse(txtCodigo.Text);
                model.Nome = txtNome.Text;
                if (ckAtivo.Checked)
                {
                    model.Ativo = true;
                }
                else
                {
                    model.Ativo = false;
                }

                if (rbFisica.Checked == true)
                {
                    model.TipoPessoa = "F"; //Fisica
                }
                else
                {
                    model.TipoPessoa = "J"; //Juridica
                }
                model.NomeFantasia = txtNomeFantasia.Text;
                model.CpfCnpj = txtCpfCnpj.Text;
                model.RgIE = txtRgIE.Text;
                model.Telefone = txtTelefone.Text;
                model.Celular = txtCelular.Text;
                model.Email = txtEmail.Text;
                model.Contato = txtContato.Text;
                model.Cep = txtCep.Text;
                model.Endereco = txtEndereco.Text;
                model.Numero = txtNumero.Text;
                model.Bairro = txtBairro.Text;
                model.CidadeId = Convert.ToInt32(cmbCidade.SelectedValue);
                model.SiteFornecedor = txtSiteFornecedor.Text;
                model.Observacao = txtObservacao.Text;


                _fornecedorService = new FornecedorService();

                if (model.FornecedorId == 0)
                {
                    _fornecedorService.Incluir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                }
                else
                {
                    _fornecedorService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.Close();
                    this.LimparCampos();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void LimparCampos()
        {
            txtDataCadastro.Text = DateTime.Now.ToString();
            txtCodigo.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtNomeFantasia.Text = string.Empty;
            ckAtivo.Checked = true;
            cmbCidade.SelectedIndex = -1;
            txtCpfCnpj.Text = string.Empty;
            txtRgIE.Text = string.Empty;
            txtCep.Text = string.Empty;
            txtEndereco.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtBairro.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            txtCelular.Text = string.Empty;
            txtSiteFornecedor.Text = string.Empty;
            txtObservacao.Text = string.Empty;
            txtContato.Text = string.Empty;
            ckAtivo.Checked = true;
            rbFisica.Checked = true;
            
        }

        private void bbiSaveAndClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                
                model = new Fornecedor();
                if (txtCodigo.Text == string.Empty)
                {
                    txtCodigo.Text = "0";
                }

                model.FornecedorId = int.Parse(txtCodigo.Text);
                model.Nome = txtNome.Text;
                if (ckAtivo.Checked)
                {
                    model.Ativo = true;
                }
                else
                {
                    model.Ativo = false;
                }

                if (rbFisica.Checked == true)
                {
                    model.TipoPessoa = "F"; //Fisica
                }
                else
                {
                    model.TipoPessoa = "J"; //Juridica
                }
                model.NomeFantasia = txtNomeFantasia.Text;
                model.CpfCnpj = txtCpfCnpj.Text;
                model.RgIE = txtRgIE.Text;
                model.Telefone = txtTelefone.Text;
                model.Celular = txtCelular.Text;
                model.Email = txtEmail.Text;
                model.Contato = txtContato.Text;
                model.Cep = txtCep.Text;
                model.Endereco = txtEndereco.Text;
                model.Numero = txtNumero.Text;
                model.Bairro = txtBairro.Text;
                model.CidadeId = Convert.ToInt32(cmbCidade.SelectedValue);
                model.SiteFornecedor = txtSiteFornecedor.Text;
                model.Observacao = txtObservacao.Text;


                _fornecedorService = new FornecedorService();

                if (model.FornecedorId == 0)
                {
                    _fornecedorService.Incluir(model);
                    FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.LimparCampos();
                    this.Close();
                }
                else
                {
                    _fornecedorService.Alterar(model);
                    FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                    this.Close();
                    this.LimparCampos();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Aviso");
            }
        }

        private void bbiLimparFormulario_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.LimparCampos();
        }

        private void bbiDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (txtCodigo.Text != string.Empty)
                {
                    if (FlyoutMessageBox.Show("Deseja realmente deletar o registro.", "Aviso",
                          MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        _fornecedorService = new FornecedorService();
                        _fornecedorService.Excluir(Convert.ToInt32(txtCodigo.Text));
                        this.LimparCampos();
                        this.Close();
                    }
                }
            }
           catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bbiPrint_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void bbiClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                      MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void FormFornecedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void FormFornecedor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter) //ENTER
            {
                try
                {
                    model = new Fornecedor();
                    if (txtCodigo.Text == string.Empty)
                    {
                        txtCodigo.Text = "0";
                    }

                    model.FornecedorId = int.Parse(txtCodigo.Text);
                    model.Nome = txtNome.Text;
                    if (ckAtivo.Checked)
                    {
                        model.Ativo = true;
                    }
                    else
                    {
                        model.Ativo = false;
                    }

                    if (rbFisica.Checked == true)
                    {
                        model.TipoPessoa = "F"; //Fisica
                    }
                    else
                    {
                        model.TipoPessoa = "J"; //Juridica
                    }
                    model.NomeFantasia = txtNomeFantasia.Text;
                    model.CpfCnpj = txtCpfCnpj.Text;
                    model.RgIE = txtRgIE.Text;
                    model.Telefone = txtTelefone.Text;
                    model.Celular = txtCelular.Text;
                    model.Email = txtEmail.Text;
                    model.Contato = txtContato.Text;
                    model.Cep = txtCep.Text;
                    model.Endereco = txtEndereco.Text;
                    model.Numero = txtNumero.Text;
                    model.Bairro = txtBairro.Text;
                    model.CidadeId = Convert.ToInt32(cmbCidade.SelectedValue);
                    model.SiteFornecedor = txtSiteFornecedor.Text;
                    model.Observacao = txtObservacao.Text;

                    _fornecedorService = new FornecedorService();

                    if (model.FornecedorId == 0)
                    {
                        _fornecedorService.Incluir(model);
                        FlyoutMessageBox.Show("Cadastro salvo com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.LimparCampos();
                        this.Close();
                    }
                    else
                    {
                        _fornecedorService.Alterar(model);
                        FlyoutMessageBox.Show("Cadastro alterado com sucesso !!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1).ToString();
                        this.Close();
                        this.LimparCampos();
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Aviso");
                }
            }
        }

        private void FormFornecedor_FormClosed(object sender, FormClosedEventArgs e)
        {
            FormFornecedorConsulta f = new FormFornecedorConsulta();
            f.RefreshGrid();
        }

        private void FormFornecedor_Load(object sender, EventArgs e)
        {
            _fornecedorService = new FornecedorService();

            cmbCidade.DataSource = _fornecedorService.CarregarCidades();
            cmbCidade.DisplayMember = "Nome";
            cmbCidade.ValueMember = "CidadeId";

            FormFornecedorConsulta f = new FormFornecedorConsulta();
            if (codigoConsultaFornecedor != 0)
            {
                _fornecedorService = new FornecedorService();
                model = new Fornecedor();
                model = _fornecedorService.BuscarPorCodigo(codigoConsultaFornecedor);

                txtDataCadastro.Text = model.DataCadastro.ToString();
                if (model.Ativo == true)
                {
                    ckAtivo.Checked = true;
                }
                else
                {
                    ckAtivo.Checked = false;
                }
                txtCodigo.Text = model.FornecedorId.ToString();
                txtNome.Text = model.Nome;
                txtNomeFantasia.Text = model.NomeFantasia;
                if (model.TipoPessoa == "F")
                {
                    rbFisica.Checked = true;
                    rbJuridica.Checked = false;
                }
                else
                {
                    rbJuridica.Checked = true;
                    rbFisica.Checked = false;
                }
                txtCpfCnpj.Text = model.CpfCnpj;
                txtRgIE.Text = model.RgIE;
                
                txtCep.Text = model.Cep;
                cmbCidade.SelectedValue = model.CidadeId;
                txtEndereco.Text = model.Endereco;
                txtNumero.Text = model.Numero;
                txtBairro.Text = model.Bairro;
                txtEmail.Text = model.Email;
                txtTelefone.Text = model.Telefone;
                txtCelular.Text = model.Celular;
                txtContato.Text = model.Contato;
                txtSiteFornecedor.Text = model.SiteFornecedor;
                txtObservacao.Text = model.Observacao;
            }
            else
            {
                lblCpfCnpj.Text = "CPF";
                lblRgIE.Text = "RG";
                txtCpfCnpj.Mask = "000,000,000-00";
                txtRgIE.Mask = "000000";
            }
        }

        private void rbFisica_CheckedChanged(object sender, EventArgs e)
        {
            if (rbFisica.Checked == true)
            {
                lblCpfCnpj.Text = "CPF";
                lblRgIE.Text = "RG";
                txtCpfCnpj.Text = string.Empty;
                txtRgIE.Text = string.Empty;
                txtCpfCnpj.Mask = "000,000,000-00";
                txtRgIE.Mask = "000000";
            }
            else
            {
               
                lblCpfCnpj.Text = "CNPJ";
                lblRgIE.Text = "IE";
                txtCpfCnpj.Mask = "00,000,000/0000-00";
                txtRgIE.Mask = "";
                txtCpfCnpj.Text = string.Empty;
                txtRgIE.Text = string.Empty;
            }
        }
    }
}
