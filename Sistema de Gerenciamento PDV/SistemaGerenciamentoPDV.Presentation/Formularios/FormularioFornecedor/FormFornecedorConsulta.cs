﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SistemaGerenciamentoPDV.Business.Service;
using DevExpress.XtraBars;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFornecedor
{
    public partial class FormFornecedorConsulta : DevExpress.XtraEditors.XtraForm
    {
        private FornecedorService _fornecedorService;
        public int codigo = 0;

        public FormFornecedorConsulta()
        {
            InitializeComponent();
        }

        private void FormFornecedorConsulta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27)) //ESC
            {
                if (FlyoutMessageBox.Show("Deseja realmente fechar o formulário.", "Aviso",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    this.Close();
                }
            }
        }

        private void windowsUIButtonPanel1_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            if(e.Button == windowsUIButtonPanel1.Buttons[0])
            {
                FormFornecedor f = new FormFornecedor();
                f.ShowDialog();
                f.Dispose();
                RefreshGrid();
            }
        }

        public void RefreshGrid()
        {
            _fornecedorService = new FornecedorService();
            gvDados.DataSource = _fornecedorService.BuscarTodos();
            gvDados.Refresh();
        }

        private void gvDados_DoubleClick(object sender, EventArgs e)
        {
            var rowHandle = gridView1.FocusedRowHandle;
            var obj = gridView1.GetRowCellValue(rowHandle, "FornecedorId");
            this.codigo = Convert.ToInt32(gridView1.GetRowCellValue(rowHandle, "FornecedorId"));

            FormFornecedor f = new FormFornecedor();
            f.codigoConsultaFornecedor = codigo;
            f.ShowDialog();

            f.Dispose();
            RefreshGrid();
        }

        private void gvDados_Load(object sender, EventArgs e)
        {
            RefreshGrid();
        }
    }
}