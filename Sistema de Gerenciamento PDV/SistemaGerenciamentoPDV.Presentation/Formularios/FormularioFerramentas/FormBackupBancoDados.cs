﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFerramentas
{
    public partial class FormBackupBancoDados : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FormBackupBancoDados()
        {
            InitializeComponent();
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.FileName = "Backup File|*.bak";
            d.ShowDialog();
            if (d.FileName != "")
            {
                //TODO TERMINA O BACKUP 
            }
        }
    }
}
