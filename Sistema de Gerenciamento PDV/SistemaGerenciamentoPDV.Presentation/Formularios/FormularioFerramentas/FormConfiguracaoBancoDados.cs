﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using SistemaGerenciamentoPDV.Business.Service;

namespace SistemaGerenciamentoPDV.Presentation.Formularios.FormularioFerramentas
{
    public partial class FormConfiguracaoBancoDados : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FormConfiguracaoBancoDados()
        {
            InitializeComponent();
            
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                StreamWriter arquivo = new StreamWriter("ConfiguracaoBanco.txt", false);
                arquivo.WriteLine(txtServidor.Text);
                arquivo.WriteLine(txtBanco.Text);
                arquivo.WriteLine(txtUsuario.Text);
                arquivo.WriteLine(txtSenha.Text);

                arquivo.Close();

                XtraMessageBox.Show("Arquivo atualizado com sucesso!!");
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message);
            }
        }

        private void FormConfiguracaoBancoDados_Load(object sender, EventArgs e)
        {
            try
            {
                StreamReader arquivo = new StreamReader("ConfiguracaoBanco.txt");
                txtServidor.Text = arquivo.ReadLine();
                txtBanco.Text = arquivo.ReadLine();
                txtUsuario.Text = arquivo.ReadLine();
                txtSenha.Text = arquivo.ReadLine();
                arquivo.Close();
            }
            catch (Exception ex)
            {

                XtraMessageBox.Show(ex.Message);
            }
        }

        private void bbiTestarConexao_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                DadosConexaoService conexao = new DadosConexaoService();
                conexao.TestarConexao(txtServidor.Text, txtBanco.Text, txtUsuario.Text, txtSenha.Text);
                XtraMessageBox.Show("Conexao efetuada com sucesso!");
            }
            catch (Exception)
            {

                XtraMessageBox.Show("Conexao invalida!");
            }
        }
    }
}
