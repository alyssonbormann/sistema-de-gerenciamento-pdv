﻿using System;
using System.Collections.Generic;
using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System.Linq;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class ProdutoRepository : RepositoryBase<Produto>, IProdutoRepository
    {
        public IEnumerable<Categoria> GetAllCategoria()
        {
            return Db.Set<Categoria>().ToList();
        }

        public IEnumerable<SubCategoria> GetAllFindSubCategoriaCodigo(int codigo)
        {
            var query = Db.Set<SubCategoria>().Where(c => c.Categoria_Codigo == codigo).ToList();
            return query;
        }

        public IEnumerable<SubCategoria> GetAllSubCategoria()
        {
            return Db.Set<SubCategoria>().ToList();
        }

        public IEnumerable<UnidadeMedida> GetAllUnidadeMedida()
        {
            return Db.Set<UnidadeMedida>().ToList();
        }
    }
}
