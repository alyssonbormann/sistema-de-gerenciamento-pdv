﻿using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class ItensCompraRepository : RepositoryBase<ItensCompra>, IItensCompraRepository
    {
        public IEnumerable<ItensCompra> GetAllItensCompraCodCompra(int codigoCompra)
        {
            return Db.Set<ItensCompra>().Where(c => c.CompraId == codigoCompra);
        }

        public ItensCompra CarregarItensCompra(int codigoItensCompra, int codigoCompra, int codigoProduto)
        {
            return Db.Set<ItensCompra>().FirstOrDefault(c => c.ItensCompraCodigo == codigoItensCompra && c.CompraId == codigoCompra && c.ProdutoId == codigoProduto);
        }

        public void ExcluirPorCodigos(ItensCompra obj)
        {
            var query = Db.Set<ItensCompra>().FirstOrDefault(c => c.ItensCompraCodigo == obj.ItensCompraCodigo && c.CompraId == obj.CompraId && c.ProdutoId == obj.ProdutoId);
            Db.Set<ItensCompra>().Remove(query);
            Db.SaveChanges();
        }

        //TODO:FAZER O INCREMENTO TMBM DA VENDA
        public void IncrementarItensCompra(int codigoProduto, double quantidade)
        {
            Produto p = Db.Set<Produto>().FirstOrDefault(c => c.Produto_Codigo == codigoProduto);
            p.Produto_Quantidade = p.Produto_Quantidade + quantidade;
            Db.Entry(p).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
        }
    }
}
