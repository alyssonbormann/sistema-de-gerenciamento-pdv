﻿using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class CompraRepository : RepositoryBase<Compra>, ICompraRepository
    {
        public IEnumerable<TipoPagamento> GetAllTipoPagamento()
        {
            return Db.Set<TipoPagamento>().ToList();
        }

        public IEnumerable<Compra> GetFindDataInicialFinal(DateTime inicial, DateTime final)
        {
            return Db.Set<Compra>().Where(c => c.CompraData >= inicial && c.CompraData <= final);
        }

        public Fornecedor GetFindFornecedorCodigo(int codigo)
        {
            return Db.Set<Fornecedor>().Find(codigo);
        }

        public Fornecedor GetFindFornecedorNome(string nome)
        {
            return Db.Set<Fornecedor>().Find(nome);
        }

        public Produto GetFindProdutoCodigo(int codigo)
        {
            return Db.Set<Produto>().Find(codigo);
        }
    }
}
