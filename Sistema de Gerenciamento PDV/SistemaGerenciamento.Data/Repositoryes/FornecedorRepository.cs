﻿using SistemaGerenciamentoPDV.Domain.Entities;
using SistemaGerenciamentoPDV.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamento.Data.Repositoryes
{
    public class FornecedorRepository : RepositoryBase<Fornecedor>, IFornecedorRepository
    {
        public IEnumerable<Cidade> GetAllCidades()
        {
            return Db.Set<Cidade>().ToList();
        }
    }
}
