﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class FornecedorService
    {
        private readonly FornecedorRepository _repository = new FornecedorRepository();

        public void Incluir(Fornecedor obj)
        {
            if (obj.Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do fornecedor é obrigatório.");
            }
            if (obj.NomeFantasia.Trim().Length == 0)
            {
                throw new Exception("O nome fantasia do fornecedor é obrigatório.");
            }
            //TODO: AJEITAR A VALIDAÇÃO DO CPFCNPJ
            if (obj.TipoPessoa == "F")
            {
                if (obj.CpfCnpj.Trim().Length == 0)
                {
                    throw new Exception("O cpf do fornecedor é obrigatório.");
                }
                if (obj.RgIE.Trim().Length == 0)
                {
                    throw new Exception("O rg do fornecedor é obrigatório.");
                }
            }
            if (obj.TipoPessoa == "J")
            {
                if (obj.CpfCnpj.Trim().Length == 0)
                {
                    throw new Exception("O cnpj do fornecedor é obrigatório.");
                }
                if (obj.RgIE.Trim().Length == 0)
                {
                    throw new Exception("O IE do fornecedor é obrigatório.");
                }
            }
          
            if (obj.Celular.Trim().Length == 0)
            {
                throw new Exception("O celular do fornecedor é obrigatório.");
            }
            if (obj.Endereco.Trim().Length == 0)
            {
                throw new Exception("O endereço do fornecedor é obrigatório.");
            }
            if (obj.Numero.Trim().Length == 0)
            {
                throw new Exception("O número do endereço do fornecedor é obrigatório.");
            }
            if (obj.Bairro.Trim().Length == 0)
            {
                throw new Exception("O bairro do fornecedor é obrigatório.");
            }
            if (obj.CidadeId == 0)
            {
                throw new Exception("A cidade do fornecedor é obrigatório.");
            }
            _repository.Insert(obj);
        }

        public void Alterar(Fornecedor obj)
        {
            if (obj.FornecedorId <= 0)
            {
                throw new Exception("O código do fornecedor é obrigatório.");
            }
            if (obj.Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do fornecedor é obrigatório.");
            }
            if (obj.NomeFantasia.Trim().Length == 0)
            {
                throw new Exception("A razão social do fornecedor é obrigatório.");
            }
            if (obj.RgIE.Trim().Length == 0)
            {
                throw new Exception("O IE do fornecedor é obrigatório.");
            }
            if (obj.CpfCnpj.Trim().Length == 0)
            {
                throw new Exception("O CNPJ do fornecedor é obrigatório.");
            }
            if (obj.Telefone.Trim().Length == 0)
            {
                throw new Exception("O telefone do cliente é obrigatório");
            }
            _repository.Update(obj);
        }

        public IEnumerable<Fornecedor> BuscarTodos()
        {
            return _repository.GetAll();
        }
        public Fornecedor BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }
        public void Excluir(int codigo)
        {
            var codigoFornecedor = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoFornecedor);
        }

        public IEnumerable<Cidade> CarregarCidades()
        {
            return _repository.GetAllCidades();
        }
    }
}
