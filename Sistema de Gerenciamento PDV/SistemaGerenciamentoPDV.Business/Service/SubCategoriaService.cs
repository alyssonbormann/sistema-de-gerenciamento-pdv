﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Business.Helpers;
using SistemaGerenciamentoPDV.Business.ViewModel;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class SubCategoriaService
    {
        private readonly SubCategoriaRepository _subCategoriaRepository = new SubCategoriaRepository();

        public void Inserir(SubCategoria subCategoria)
        {
            
            var erros = Validacao.getValidationErros(subCategoria);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }

            _subCategoriaRepository.Insert(subCategoria);
        }
        public void Alterar(SubCategoria subCategoria)
        {
            if (subCategoria.SubCategoria_Codigo <= 0)
            {
                throw new Exception("Código da subcategoria obrigatório");
            }
            if (subCategoria.SubCategoria_Nome.Trim().Length == 0)
            {
                throw new Exception("Nome da subcategoria obrigatório.");
            }
            if (subCategoria.Categoria_Codigo <= 0)
            {
                throw new Exception("Informe a categoria.");
            }
            _subCategoriaRepository.Update(subCategoria);
        }
        public void Excluir(int codigo)
        {
            var codigoSubCategoria = _subCategoriaRepository.GetFindCodigo(codigo);
            _subCategoriaRepository.Delete(codigoSubCategoria);
        }

        public IEnumerable<SubCategoria> BuscarTodos()
        {
            return _subCategoriaRepository.GetAll();
        }

        public SubCategoria BuscarPorCodigo(int codigo)
        {
            return _subCategoriaRepository.GetFindCodigo(codigo);
        }

        public IEnumerable<Categoria> BuscarCategorias()
        {
            return _subCategoriaRepository.GetAllCategoria().OrderBy(c => c.Categoria_Nome);
        }

        public List<SubCategoriaViewModel> BuscarPorNomeSubCategoria(string valor)
        {
            if (valor != string.Empty)
            {
                var query = from sub in _subCategoriaRepository.GetAll()
                            join cat in _subCategoriaRepository.GetAllCategoria() on sub.Categoria_Codigo equals cat.Categoria_Codigo
                            where sub.SubCategoria_Nome.ToUpper().Contains(valor.ToUpper())
                            select new SubCategoriaViewModel
                            {
                                SubCategoria_Codigo = sub.Categoria_Codigo,
                                SubCategoria_Nome = sub.SubCategoria_Nome,
                                Categoria_Codigo = cat.Categoria_Codigo,
                                Categoria_Nome = cat.Categoria_Nome
                            };

                return query.ToList();
            }
            else
            {
                var query = from sub in _subCategoriaRepository.GetAll()
                            join cat in _subCategoriaRepository.GetAllCategoria() on sub.Categoria_Codigo equals cat.Categoria_Codigo
                            select new SubCategoriaViewModel
                            {
                                SubCategoria_Codigo = sub.SubCategoria_Codigo,
                                SubCategoria_Nome = sub.SubCategoria_Nome,
                                Categoria_Codigo = cat.Categoria_Codigo,
                                Categoria_Nome = cat.Categoria_Nome
                            };
                return query.ToList();
            }
        }
    }
}
