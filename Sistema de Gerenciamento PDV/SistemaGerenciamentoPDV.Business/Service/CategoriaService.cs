﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Business.Helpers;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class CategoriaService
    {
        private readonly CategoriaRepository _categoriaRepository = new CategoriaRepository();

        public void Inserir(Categoria categoria)
        {
            //Primeiro verifico os campos requeeridos
            var erros = Validacao.getValidationErros(categoria);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }

            //Depois dos campos verificados, insiro os dados
            //Se ocorrer algum error, eu sei que não é por validações
            try
            {
                _categoriaRepository.Insert(categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }


        public void Alterar(Categoria categoria)
        {
            //Primeiro verifico os campos requeridos
            var erros = Validacao.getValidationErros(categoria);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }

            //Depois dos campos verificados, insiro os dados
            //Se ocorrer algum error, eu sei que não é por validações
            try
            {
                _categoriaRepository.Update(categoria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Excluir(int codigo)
        {
            var codigoCategoria = _categoriaRepository.GetFindCodigo(codigo);
            _categoriaRepository.Delete(codigoCategoria);
        }

        public IEnumerable<Categoria> BuscarTodos()
        {
            return _categoriaRepository.GetAll();
        }

        public Categoria BuscarPorCodigo(int codigo)
        {
            return _categoriaRepository.GetFindCodigo(codigo);
        }

    }
}
