﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Business.Helpers;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class ParcelasCompraService
    {
        readonly private ParcelasCompraRepository _repository = new  ParcelasCompraRepository();

        public void Incluir(ParcelasCompra obj)
        {
            var erros = Validacao.getValidationErros(obj);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }
            _repository.Insert(obj);
        }

        public void Alterar(ParcelasCompra obj)
        {
            var erros = Validacao.getValidationErros(obj);
            foreach(var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }
            _repository.Update(obj);
        }
        
    }
}
