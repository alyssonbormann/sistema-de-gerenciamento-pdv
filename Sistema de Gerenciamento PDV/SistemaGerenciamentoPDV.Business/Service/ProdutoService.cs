﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Business.ViewModel;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class ProdutoService
    {
        private readonly ProdutoRepository _repository = new ProdutoRepository();

        public void Inserir(Produto produto)
        {
            if (produto.Produto_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do produto é obrigatório.");
            }
            if (produto.Produto_ValorVenda <= 0)
            {
                throw new Exception("O valor da venda do produto é obrigatório.");
            }
            if (produto.Produto_Quantidade < 0)
            {
                throw new Exception("A quantidade do produto deve ser maior ou igual a zero.");
            }
            if (produto.Categoria_Codigo <= 0)
            {
                throw new Exception("O código da categoria é obrigatório");
            }
            if (produto.SubCategoria_Codigo <= 0)
            {
                throw new Exception("O código da subcategoria é obrigatório");
            }
            if (produto.UnidadeMedida_Codigo <= 0)
            {
                throw new Exception("O código da unidade de medida é obrigatório");
            }

            _repository.Insert(produto);
        }

        public void Alterar(Produto produto)
        {
            if (produto.Produto_Codigo <= 0)
            {
                throw new Exception("O código do produto é obrigatório");
            }
            if (produto.Produto_Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do produto é obrigatório.");
            }
            if (produto.Produto_ValorVenda <= 0)
            {
                throw new Exception("O valor da venda do produto é obrigatório.");
            }
            if (produto.Produto_Quantidade <= 0)
            {
                throw new Exception("A quantidade do produto deve ser maior ou igual a zero.");
            }
            if (produto.Categoria_Codigo <= 0)
            {
                throw new Exception("O código da categoria é obrigatório");
            }
            if (produto.SubCategoria_Codigo <= 0)
            {
                throw new Exception("O código da subcategoria é obrigatório");
            }
            if (produto.UnidadeMedida_Codigo <= 0)
            {
                throw new Exception("O código da unidade de medida é obrigatório");
            }

            _repository.Update(produto);
        }
        public void Excluir(int codigo)
        {
            var codigoProduto = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoProduto);
        }

        public IEnumerable<Produto> BuscarTodos()
        {
            return _repository.GetAll(); 
        }
        public Produto BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }
        public List<ProdutoViewModel> BuscarProdutoPorNome(string valor)
        {
            if (valor != string.Empty)
            {
                var query = from pro in _repository.GetAll()
                            join cat in _repository.GetAllCategoria() on pro.Categoria_Codigo equals cat.Categoria_Codigo
                            join subCat in _repository.GetAllSubCategoria() on pro.SubCategoria_Codigo equals subCat.SubCategoria_Codigo
                            join uni in _repository.GetAllUnidadeMedida() on pro.UnidadeMedida_Codigo equals uni.UnidadeMedida_Codigo
                            where pro.Produto_Nome.ToUpper().Contains(valor.ToUpper())
                            select new ProdutoViewModel
                            {
                                Produto_Codigo = pro.Produto_Codigo,
                                Produto_Nome = pro.Produto_Nome,
                                Produto_Descricao = pro.Produto_Descricao,
                                //Produto_Foto = pro.Produto_Foto,
                                Produto_ValorPago = Math.Round(pro.Produto_ValorPago, 2),
                                Produto_ValorVenda = Math.Round(pro.Produto_ValorVenda, 2),
                                Produto_Quantidade = (double)pro.Produto_Quantidade,
                                //UnidadeMedida_Codigo = pro.UnidadeMedida_Codigo,
                                UnidadeMedida_Nome = uni.UnidadeMedida_Nome,
                                //Categoria_Codigo = pro.Categoria_Codigo,
                                Categoria_Nome = cat.Categoria_Nome,
                                //SubCategoria_Codigo = pro.SubCategoria_Codigo,
                                SubCategoria_Nome = subCat.SubCategoria_Nome
                            };
                return query.ToList();
            }
            else
            {
                var query = from pro in _repository.GetAll()
                            join cat in _repository.GetAllCategoria() on pro.Categoria_Codigo equals cat.Categoria_Codigo
                            join subCat in _repository.GetAllSubCategoria() on pro.SubCategoria_Codigo equals subCat.SubCategoria_Codigo
                            join uni in _repository.GetAllUnidadeMedida() on pro.UnidadeMedida_Codigo equals uni.UnidadeMedida_Codigo
                            select new ProdutoViewModel
                            {
                                Produto_Codigo = pro.Produto_Codigo,
                                Produto_Nome = pro.Produto_Nome,
                                Produto_Descricao = pro.Produto_Descricao,
                                //Produto_Foto = pro.Produto_Foto,
                                Produto_ValorPago = Math.Round(pro.Produto_ValorPago,2),
                                Produto_ValorVenda = Math.Round(pro.Produto_ValorVenda,2),
                                Produto_Quantidade = pro.Produto_Quantidade,
                                //UnidadeMedida_Codigo = pro.UnidadeMedida_Codigo,
                                UnidadeMedida_Nome = uni.UnidadeMedida_Nome,
                                //Categoria_Codigo = pro.Categoria_Codigo,
                                Categoria_Nome = cat.Categoria_Nome,
                                //SubCategoria_Codigo = pro.SubCategoria_Codigo,
                                SubCategoria_Nome = subCat.SubCategoria_Nome
                            };
                return query.ToList();
            }
        }

        public IEnumerable<UnidadeMedida> CarregarUnidadeMedida()
        {
            return _repository.GetAllUnidadeMedida();
        }
        public IEnumerable<SubCategoria> CarregarSubCategoria()
        {
            return _repository.GetAllSubCategoria();
        }

        public IEnumerable<Categoria> CarregarCategorias()
        {
            return _repository.GetAllCategoria();
        }

        public IEnumerable<SubCategoria> CarregarSubCategoriaCodigo(int codigo)
        {
            return _repository.GetAllFindSubCategoriaCodigo(codigo);
        }

    }
}
