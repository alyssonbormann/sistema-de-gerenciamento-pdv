﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class CompraService
    {
        private readonly CompraRepository _repository = new CompraRepository();

        //TODO: VALIDAR OS OBJTOS
        public void Incluir(Compra obj)
        {
            if (obj.CompraData == null)
            {
                throw new Exception("A data é obrigatório.");
            }
            if (obj.NumeroNotalFiscal == null && obj.NumeroNotalFiscal <= -1)
            {
                throw new Exception("O número da nota fiscal é obrigatório.");
            }
            if (obj.FornecedorId <= 0)
            {
                throw new Exception("O fornecedor é obrigatório.");
            }
            if (obj.TipoPagamento <= 0)
            {
                throw new Exception("O tipo de pagamento é obrigatório.");
            }
            _repository.Insert(obj);
        }

        public void Alterar(Compra obj)
        {
            if (obj.CompraData == null)
            {
                throw new Exception("A data é obrigatório.");
            }
            if (obj.NumeroNotalFiscal == null && obj.NumeroNotalFiscal <= -1)
            {
                throw new Exception("O número da nota fiscal é obrigatório.");
            }
            if (obj.FornecedorId <= 0)
            {
                throw new Exception("O fornecedor é obrigatório.");
            }
            _repository.Update(obj);
        }

        public IEnumerable<Compra> BuscarTodos()
        {
            return _repository.GetAll();
        }

        public Compra BuscarPorCodigo(int codigo)
        {
            return _repository.GetFindCodigo(codigo);
        }

        public void Excluir(int codigo)
        {
            var codigoCompra = _repository.GetFindCodigo(codigo);
            _repository.Delete(codigoCompra);
        }
        
        //TODO: TALVEZ HAJA ERRO AQUI NOS 3 METODOS
        public Fornecedor BuscarPorCodigoFornecedor(int fornecedor)
        {
            return _repository.GetFindFornecedorCodigo(fornecedor);
        }

        public Fornecedor BuscarNomeFornecedor(string nome)
        {
            return _repository.GetFindFornecedorNome(nome);
        }

        public IEnumerable<Compra> BuscarPorData(DateTime inicial, DateTime final)
        {
            return _repository.GetFindDataInicialFinal(inicial, final);
        }

        public Produto BuscarPorCodigoProduto(int codigo)
        {
            return _repository.GetFindProdutoCodigo(codigo);
        }

        public IEnumerable<TipoPagamento> CarregarTipoPagamento()
        {
            return _repository.GetAllTipoPagamento();
        }
    }
}
