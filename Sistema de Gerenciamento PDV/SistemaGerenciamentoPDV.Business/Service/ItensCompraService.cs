﻿using SistemaGerenciamento.Data.Repositoryes;
using SistemaGerenciamentoPDV.Business.Helpers;
using SistemaGerenciamentoPDV.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class ItensCompraService
    {
        private readonly ItensCompraRepository _repository = new ItensCompraRepository();

        public void Incluir(ItensCompra obj)
        {
            ItensCompraRepository _r = new ItensCompraRepository();//criar uma nova instancia para poder incluir a lista de itens
            var erros = Validacao.getValidationErros(obj);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }

            _r.Insert(obj);
        }

        public void Alterar(ItensCompra obj)
        {
            var erros = Validacao.getValidationErros(obj);
            foreach (var error in erros)
            {
                throw new Exception("" + error.ErrorMessage);
            }

            try
            {
                _repository.Update(obj);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public void ExcluirPorCodigos(ItensCompra obj)
        {
            //TODO: EXCLUIR ITENS DA COMPRA PASSANDO COMO PARAMETRO UM MODELO
            //AULA 86 15:43
            _repository.ExcluirPorCodigos(obj);
        }

        public IEnumerable<ItensCompra> BuscarItensCompraCodigoCompra(int codigoCompra)
        {
            return _repository.GetAllItensCompraCodCompra(codigoCompra);
        }

        public void IncrementarItensCompra(int codigoProduto, double quantidade)
        {
            _repository.IncrementarItensCompra(codigoProduto, quantidade);
        }
    }
}
