﻿using SistemaGerenciamento.Data.EntityContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.Service
{
    public class DadosConexaoService
    {
        public static String servidor;
        public static String banco;
        public static String usuario;
        public static String senha;
        public void TestarConexao(String servidor,String banco,String usuario,String senha)
        {
            DadosConexao.servidor = servidor;
            DadosConexao.banco = banco;
            DadosConexao.usuario = usuario;
            DadosConexao.senha = senha;

            SqlConnection c = new SqlConnection();
            c.ConnectionString = DadosConexao.StringConexao;
            c.Open();
            c.Close();
        }
        
    }
    //TODO:AGEITAR A CONFIGURAÇÃO DO BANCO DE DADOS
}
