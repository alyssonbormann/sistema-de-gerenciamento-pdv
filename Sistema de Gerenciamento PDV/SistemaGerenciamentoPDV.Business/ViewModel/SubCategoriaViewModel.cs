﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaGerenciamentoPDV.Business.ViewModel
{
    public class SubCategoriaViewModel
    {
        [Display(Name = "Código")]
        public int SubCategoria_Codigo { get; set; }

        [Display(Name = "SubCategoria")]
        public string SubCategoria_Nome { get; set; }

        [Display(Name = "Código Categoria")]
        public int Categoria_Codigo { get; set; }

        [Display(Name = "Categoria")]
        public string Categoria_Nome { get; set; }
    }
}
